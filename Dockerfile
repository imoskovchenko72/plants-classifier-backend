FROM python:3.11-slim

ENV \
  PYTHONFAULTHANDLER=1 \
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.5.1 \
  POETRY_VIRTUALENVS_CREATE=false \
  POETRY_CACHE_DIR='/var/cache/pypoetry'


RUN apt-get update && apt-get install -y --no-install-recommends \
    git-core \
    make \
    && apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
    && pip install "poetry==$POETRY_VERSION"


WORKDIR /app
COPY ./poetry.lock ./pyproject.toml /app/

RUN poetry install --no-interaction --no-ansi --no-dev

COPY ./entrypoint.sh /app/entrypoint.sh
COPY ./plants_classifier /app/plants_classifier

RUN chmod +x entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
