import os
from pathlib import Path
from typing import Generator
from uuid import uuid4

import pytest
import sqlalchemy
from alembic.command import upgrade
from alembic.config import Config
from configargparse import Namespace
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy_utils import create_database, database_exists, drop_database

from plants_classifier.common.postgres import get_postgres_url
from plants_classifier.core.settings import get_config, init_config

PROJECT_PATH = Path(__file__).parent.parent.resolve()


def make_alembic_config(cmd_opts: Namespace, base_path: Path = PROJECT_PATH) -> Config:
    """
    Creates an alembic configuration object based on command line arguments.
    Replaces relative paths with absolute ones.
    """
    path_to_folder = cmd_opts.config
    if not os.path.isabs(cmd_opts.config):
        cmd_opts.config = os.path.join(base_path, cmd_opts.config + "alembic.ini")

    config = Config(file_=cmd_opts.config, ini_section=cmd_opts.name, cmd_opts=cmd_opts)

    alembic_location = config.get_main_option("script_location")
    if alembic_location is None:
        raise ValueError("bad alembic location")
    if not os.path.isabs(alembic_location):
        config.set_main_option(
            "script_location",
            os.path.join(base_path, path_to_folder + alembic_location),
        )
    if cmd_opts.pg_url:
        config.set_main_option("sqlalchemy.url", cmd_opts.pg_url)

    return config


@pytest.fixture
def initialized_config():
    init_config()

    return get_config()


@pytest.fixture
def postgres(initialized_config) -> Generator[str, None, None]:
    """
    Creates temporary database for each test
    """
    from plants_classifier.common.postgres import get_postgres_url

    tmp_name = ".".join([uuid4().hex, "pytest"])

    initialized_config.db.postgres.database = tmp_name

    tmp_url = get_postgres_url()
    if not database_exists(get_postgres_url()):
        create_database(get_postgres_url())

    try:
        yield tmp_url
    finally:
        drop_database(tmp_url)


@pytest.fixture
def alembic_config(postgres) -> Config:
    """
    Creates alembic config file.
    """
    cmd_options = Namespace(
        config="plants_classifier/migrations/",
        name="alembic",
        pg_url=postgres,
        raiseerr=False,
        x=None,
    )
    return make_alembic_config(cmd_options)


@pytest.fixture(scope="function")
def migrated_postgres(alembic_config: Config) -> None:
    """
    Makes database migrations
    """
    upgrade(alembic_config, "head")


@pytest.fixture(scope="function")
def db_session() -> Session:
    engine = sqlalchemy.create_engine(url=get_postgres_url())
    session_maker = sessionmaker(engine, expire_on_commit=True)
    with session_maker() as session:
        try:
            yield session
        except Exception:
            session.rollback()
        else:
            session.commit()
