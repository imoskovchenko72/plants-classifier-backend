import random
import time

from sqlalchemy.orm import Session

from plants_classifier.core.classification import get_most_relevant_plants
from plants_classifier.core.constants import PlantCharacteristicType
from plants_classifier.core.db_models import PlantModel
from plants_classifier.core.dto import PlantCharacteristics
from plants_classifier.core.helpers import get_plant_characteristics_from_db_model
from plants_classifier.core.repositories import (
    CharacteristicsRepository,
    PlantRepository,
)

random.seed(12398754)  # to make tests determined


class TestPlantsClassificationSpeed:
    MEASUREMENTS_PER_TEST = 50

    def test_speed(self, migrated_postgres):
        plants = PlantRepository().all()
        time_sum = 0

        for _ in range(self.MEASUREMENTS_PER_TEST):
            plant = random.choice(plants)

            start_time = time.time()

            get_most_relevant_plants(get_plant_characteristics_from_db_model(plant))

            end_time = time.time()
            time_delta = end_time - start_time

            assert time_delta <= 0.5
            time_sum += time_delta

        print(
            f"Average time for classification for random plant from db is {time_sum / self.MEASUREMENTS_PER_TEST}"
        )

    def test_speed_on_max_characteristics_count(self, migrated_postgres):
        plant = PlantCharacteristics()

        characteristics_repository = CharacteristicsRepository()
        for characteristic in PlantCharacteristicType:
            setattr(
                plant,
                characteristic.value,
                characteristics_repository.get_all_by_characteristic_type(
                    characteristic
                ),
            )

        start_time = time.time()

        get_most_relevant_plants(plant)

        end_time = time.time()
        time_delta = end_time - start_time

        assert time_delta <= 1

        print(
            f"Time for classification of plant with all known characteristics values is {time_delta}"
        )


class TestPlantsClassificationQuality:
    MEASUREMENTS_PER_TEST = 50

    def test_classify_plant_from_db(self, migrated_postgres, db_session):
        max_place = 100
        plants = self._get_plants_with_characteristics(db_session=db_session)

        for _ in range(self.MEASUREMENTS_PER_TEST):
            plant = random.choice(plants)
            plant_characteristics = get_plant_characteristics_from_db_model(plant)

            most_relevant_plants = get_most_relevant_plants(plant_characteristics)

            place = None
            for i, maybe_desired_plant in enumerate(most_relevant_plants):
                if maybe_desired_plant.id == plant.id:
                    place = i
                    break

            assert (
                place is not None and place < max_place
            ), f"Plant: id={plant.id}, name={plant.name}"
            print(f"[PLANT AS IS] Plant {plant.name} found on {place + 1} place")

    def test_classify_plant_with_only_one_life_form(
        self, migrated_postgres, db_session
    ):
        max_place = 1000
        plants = self._get_plants_with_characteristics(db_session=db_session)

        for _ in range(self.MEASUREMENTS_PER_TEST):
            plant = random.choice(plants)
            plant_characteristics = get_plant_characteristics_from_db_model(plant)

            if not plant_characteristics.life_form:
                continue

            plant_characteristics.life_form = [
                random.choice(plant_characteristics.life_form)
            ]

            most_relevant_plants = get_most_relevant_plants(plant_characteristics)

            place = None
            for i, maybe_desired_plant in enumerate(most_relevant_plants):
                if maybe_desired_plant.id == plant.id:
                    place = i
                    break

            assert (
                place is not None and place < max_place
            ), f"Plant: id={plant.id}, name={plant.name}"
            print(
                f"[PLANT WITH 1 LIFE FORM] Plant {plant.name} found on {place + 1} place"
            )

    def test_classify_without_some_characteristics(self, migrated_postgres, db_session):
        plants = self._get_plants_with_characteristics(db_session=db_session)

        for _ in range(self.MEASUREMENTS_PER_TEST):
            plant = random.choice(plants)
            plant_characteristics = get_plant_characteristics_from_db_model(plant)

            for characteristic in PlantCharacteristicType:
                characteristic_value = plant_characteristics.model_dump().get(
                    characteristic.value
                )
                if characteristic_value is not None:
                    setattr(
                        plant_characteristics,
                        characteristic.value,
                        self._remove_some_elements_with_probability(
                            characteristic_value, 0.2
                        ),
                    )

            most_relevant_plants = get_most_relevant_plants(plant_characteristics)

            place = None
            for i, maybe_desired_plant in enumerate(most_relevant_plants):
                if maybe_desired_plant.id == plant.id:
                    place = i
                    break

            assert (
                place is not None and place < 1000
            ), f"Plant: id={plant.id}, name={plant.name}"
            print(
                f"[PLANT WITHOUT SOME CHARACTERISTICS] Plant {plant.name} found on {place + 1} place"
            )

    def test_classify_with_some_characteristics_modifications(
        self, migrated_postgres, db_session
    ):
        max_place = 1000
        plants = self._get_plants_with_characteristics(db_session=db_session)

        for _ in range(self.MEASUREMENTS_PER_TEST):
            plant = random.choice(plants)
            plant_characteristics = get_plant_characteristics_from_db_model(plant)

            for characteristic in PlantCharacteristicType:
                characteristic_value = plant_characteristics.model_dump().get(
                    characteristic.value
                )
                if characteristic_value is not None:
                    updated_characteristic = (
                        self._remove_some_elements_with_probability(
                            characteristic_value, 0.2
                        )
                    )
                    updated_characteristic = (
                        self._add_some_characteristics_wit_probability(
                            characteristic_type=characteristic,
                            elements=updated_characteristic,
                            addition_probability=0.2,
                            db_session=db_session,
                        )
                    )
                    setattr(
                        plant_characteristics,
                        characteristic.value,
                        updated_characteristic,
                    )

            most_relevant_plants = get_most_relevant_plants(plant_characteristics)

            place = None
            for i, maybe_desired_plant in enumerate(most_relevant_plants):
                if maybe_desired_plant.id == plant.id:
                    place = i
                    break

            assert (
                place is not None and place < max_place
            ), f"Plant: id={plant.id}, name={plant.name}"
            print(
                f"[PLANT WITH SOME MODIFICATIONS] Plant {plant.name} found on {place + 1} place"
            )

    @staticmethod
    def _remove_some_elements_with_probability(
        elements: list[str], remove_probability: float
    ) -> list[str]:
        return [element for element in elements if random.random() > remove_probability]

    @staticmethod
    def _add_some_characteristics_wit_probability(
        characteristic_type: PlantCharacteristicType,
        elements: list[str],
        addition_probability,
        db_session: Session,
    ) -> list[str]:
        elements_set = set(elements)
        for (
            characteristic
        ) in CharacteristicsRepository().get_all_by_characteristic_type(
            characteristic_type, db_session=db_session
        ):
            if (
                characteristic not in elements_set
                and random.random() > addition_probability
            ):
                elements_set.add(characteristic)

        return list(elements_set)

    @staticmethod
    def _get_plants_with_characteristics(db_session: Session) -> list[PlantModel]:
        plants = PlantRepository().all(db_session=db_session)

        return [
            plant
            for plant in plants
            if any(get_plant_characteristics_from_db_model(plant).model_dump().values())
        ]
