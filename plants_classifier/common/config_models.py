from typing import Optional

from pydantic import BaseModel

from .file_storage.file_storage_config import (
    FileStorageConfig,
    _init_file_storage_config,
)
from .postgres.postgres_config import PostgresConfig, _init_postgres_config


class LoggerConfig(BaseModel):
    level: str


_logger_config: Optional[LoggerConfig] = None


def init_common_config_models(
    logger_config_value: LoggerConfig,
    postgres_config_value: PostgresConfig,
    file_storage_config_value: FileStorageConfig,
):
    global _logger_config
    _logger_config = logger_config_value
    _init_postgres_config(postgres_config_value)
    _init_file_storage_config(file_storage_config_value)


def get_logger_config() -> LoggerConfig:
    global _logger_config
    if _logger_config is None:
        raise ValueError("Logger config is not initialized.")
    return _logger_config
