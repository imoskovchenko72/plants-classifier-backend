import json
import os
from typing import Any

import yaml
from pydantic import BaseModel

_SUPPORTED_CONFIG_PARSERS = {
    "yaml": yaml.safe_load,
    "yml": yaml.safe_load,
    "json": json.load,
}


class ConfigFactory:
    @classmethod
    def create_config_object(
        cls, model_type: type[BaseModel], config_paths: tuple[str]
    ) -> BaseModel:
        """
        Creates pydantic model object described in several files

        :param model_type: some pydantic model type to fill
        :param config_paths: paths to files with config data

        :return: filled pydantic model object

        :raises:
            TypeError:
                if config files has unsupported format or has incorrect data
        """
        config_data: dict[str, Any] = dict()
        for path in config_paths:
            file_format = os.path.split(path)[-1].split(".")[-1].lower()
            if file_format not in _SUPPORTED_CONFIG_PARSERS:
                raise TypeError(
                    f"Unexpected config file type. Supported formats: {', '.join(_SUPPORTED_CONFIG_PARSERS.keys())}"
                )

            with open(path, "r") as file:
                try:
                    data = _SUPPORTED_CONFIG_PARSERS[file_format](file)
                except Exception:
                    raise TypeError(f"Some error occurred during parsing file {path}")
            cls._extend_dict_keys(config_data, data)
        return model_type.model_validate(config_data)

    @classmethod
    def _extend_dict_keys(cls, a: dict[str, Any], b: dict[str, Any]) -> None:
        """
        Recursively adds keys from dict b to dict a

        :param a: expandable dict
        :param b: dict with added keys

        :raises:
            KeyError:
                if some identical keys have different values
        """

        for key, value in b.items():
            if key not in a:
                a[key] = value
            else:
                if isinstance(a[key], dict) and isinstance(value, dict):
                    cls._extend_dict_keys(a[key], value)
                elif a[key] != value:
                    raise KeyError(
                        f"Unable to set to key {key} different values: {a[key]} and {value}"
                    )
