import logging
from datetime import datetime
from typing import Any

from pythonjsonlogger import jsonlogger

from .config_models import get_logger_config

DEFAULT_LOG_LEVEL = logging.DEBUG


class _JsonFormatter(jsonlogger.JsonFormatter):
    def __init__(self, logger_name: str):
        super().__init__()
        self.logger_name = logger_name

    def add_fields(
        self,
        log_record: dict[str, Any],
        record: logging.LogRecord,
        message_dict: dict[str, Any],
    ):
        super(_JsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get("timestamp"):
            now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            log_record["timestamp"] = now

        if not log_record.get("level"):
            log_record["level"] = record.levelname

        if not log_record.get("handler"):
            log_record["handler"] = self.logger_name


class AppLogger:
    def __init__(self, name: str):
        self._logger = logging.getLogger(name)

        log_level = DEFAULT_LOG_LEVEL
        if get_logger_config().level.lower() == "debug":
            log_level = logging.DEBUG
        if get_logger_config().level.lower() == "info":
            log_level = logging.INFO
        elif get_logger_config().level.lower() == "warn":
            log_level = logging.WARN
        elif get_logger_config().level.lower() == "error":
            log_level = logging.ERROR
        elif get_logger_config().level.lower() == "fatal":
            log_level = logging.FATAL
        self._logger.level = log_level

        log_handler = logging.StreamHandler()
        log_handler.setFormatter(_JsonFormatter(name))

        self._logger.addHandler(log_handler)

    def debug(self, message: str, **kwargs):
        self._logger.debug(message, extra=kwargs)

    def info(self, message: str, **kwargs):
        self._logger.info(message, extra=kwargs)

    def warn(self, message: str, **kwargs):
        self._logger.warning(message, extra=kwargs)

    def error(self, message: str, **kwargs):
        self._logger.error(message, extra=kwargs)

    def fatal(self, message: str, **kwargs):
        self._logger.fatal(message, extra=kwargs)
