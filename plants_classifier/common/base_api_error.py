from functools import lru_cache
from http import HTTPStatus
from typing import Optional

from pydantic import BaseModel


class BaseApiError(Exception):
    status_code: int = HTTPStatus.INTERNAL_SERVER_ERROR
    id: str = "INTERNAL_ERROR"
    message: str = "Some internal error occurred."

    def __init__(self, message: Optional[str] = None):
        if message is not None:
            self.message = message

    @classmethod
    @lru_cache  # to not generate dto for same classes twice
    def _to_pydantic_model(cls) -> type[BaseModel]:
        class Model(BaseModel):
            id: str = cls.id
            message: str = cls.message

        return Model


class BaseBadRequestError(BaseApiError):
    status_code = HTTPStatus.BAD_REQUEST
    id = "BAD_REQUEST_ERROR"
    message = "Bad request."


class BaseUnauthorizedError(BaseApiError):
    status_code = HTTPStatus.UNAUTHORIZED
    id = "UNAUTHORIZED_ERROR"
    message = "Unauthorized."


class BaseForbiddenError(BaseApiError):
    status_code = HTTPStatus.FORBIDDEN
    id = "FORBIDDEN_ERROR"
    message = "Access denied."


class BaseNotFoundError(BaseApiError):
    status_code = HTTPStatus.NOT_FOUND
    id = "NOT_FOUND_ERROR"
    message = "Resource not found."
