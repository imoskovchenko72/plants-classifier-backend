import uuid

from fastapi import Header
from pydantic import BaseModel, Field


class BaseRequest(BaseModel):
    request_id: uuid.UUID = Field(Header(default_factory=uuid.uuid4))
