from copy import deepcopy
from typing import Any

from pydantic import BaseModel
from sqlalchemy import MetaData
from sqlalchemy.orm import declarative_base

from .base_request import BaseRequest

metadata = MetaData()
DeclarativeBase = declarative_base(metadata=metadata)


class BaseDbModel(DeclarativeBase):
    """
    Don't forget to import all inherited dto into alembic env file to autogenerate migrations
    """

    __abstract__ = True

    @classmethod
    def from_base_model(cls, model: BaseModel) -> "BaseDbModel":
        model_dict = model.model_dump()
        return cls(**model_dict)

    @classmethod
    def from_request(cls, request: BaseRequest) -> "BaseDbModel":
        model_dict = request.model_dump()
        model_dict.pop("request_id", None)
        return cls(**model_dict)

    @classmethod
    def from_dict(cls, model_dict: dict[str, Any]) -> "BaseDbModel":
        return cls(**model_dict)

    def to_dict(self) -> dict[str, Any]:
        model_dict = deepcopy(self.__dict__)
        model_dict.pop("_sa_instance_state", None)
        return model_dict
