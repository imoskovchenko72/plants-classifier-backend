from http import HTTPMethod, HTTPStatus
from typing import Any, Callable, Optional

from .base_api_error import BaseApiError
from .base_request import BaseRequest
from .base_response import BaseResponse


class Handler:
    def __init__(
        self,
        path: str,
        method: HTTPMethod,
        view: Callable,
        request_model: Optional[type[BaseRequest]],
        response_model: Optional[type[BaseResponse]],
        status_code: Optional[HTTPStatus] = HTTPStatus.OK,
        error_models: Optional[list[type[BaseApiError]]] = None,
        tags: Optional[list[str]] = None,
        name: Optional[str] = None,
        description: Optional[str] = None,
        disabled: bool = False,
        request_to_dto_mapping: Optional[Callable[[BaseRequest], Any]] = None,
        dto_to_response_mapping: Optional[Callable[[Any], BaseResponse]] = None,
    ):
        self.path = path
        self.method = method
        self.view = view
        self.request_model = request_model
        self.response_model = response_model
        self.status_code = status_code
        self.error_models = error_models
        self.tags = tags
        self.name = name
        self.description = description
        self.disabled = disabled
        self.request_to_dto_mapping = request_to_dto_mapping
        self.dto_to_response_mapping = dto_to_response_mapping

        _HANDLERS_REGISTRY.append(self)


_HANDLERS_REGISTRY: list[Handler] = list()
