from typing import Any, Callable, Optional

from fastapi import Depends, FastAPI, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from .base_api_error import BaseApiError
from .base_request import BaseRequest
from .base_response import BaseResponse
from .handler import _HANDLERS_REGISTRY
from .logger import AppLogger

logger = AppLogger(__name__)


def _base_api_error_handler(_, error: BaseApiError) -> JSONResponse:
    return JSONResponse(
        status_code=error.status_code,
        content={"error_id": error.id, "message": error.message},
    )


def _default_handler_view_decorator(
    func: Callable,
    request_model: Optional[type[BaseRequest]],
    response_model: Optional[type[BaseResponse]],
    request_to_dto_mapping: Optional[Callable[[BaseRequest], Any]] = None,
    dto_to_response_mapping: Optional[Callable[[Any], BaseResponse]] = None,
) -> Callable:
    input_dto_type: type = func.__annotations__.get("request", None)
    output_dto_type: type = func.__annotations__.get("return", None)

    def _default_request_to_dto_mapping(request: BaseRequest) -> input_dto_type:
        if input_dto_type is None:
            return
        request_dict = request.model_dump(mode="json")
        request_dict.pop("request_id")
        return input_dto_type(**request_dict, ignore_extra_fields=True)

    def _default_dto_to_response_mapping(dto: output_dto_type) -> response_model:
        if output_dto_type is None:
            return
        dto_dict = dto.model_dump(mode="json")
        return response_model(**dto_dict, ignore_extra_fields=True)

    request_to_dto_mapping = request_to_dto_mapping or _default_request_to_dto_mapping
    dto_to_response_mapping = (
        dto_to_response_mapping or _default_dto_to_response_mapping
    )

    # functools.wraps not used here in order not to change signature
    # changed __name__ attribute instead
    def wrapper(response: Response, request: request_model = Depends()) -> Any:
        logger.info("Request registered", request_id=request.request_id)
        response.headers["request-id"] = str(request.request_id)
        try:
            kwargs = {"request": request_to_dto_mapping(request)}
            if "response" in func.__annotations__:
                kwargs["response"] = kwargs
            result = func(**kwargs)
            result_response_model = dto_to_response_mapping(result)

            logger.info("Request succeeded", request_id=request.request_id)
            return result_response_model
        except BaseApiError as e:
            logger.error(
                f"Request finished with error: {e.message}", stacktrace=e.__traceback__
            )
            raise
        except Exception as e:
            logger.error(
                f"Request finished with error: {e.__str__()}",
                stacktrace=e.__traceback__,
            )
            raise BaseApiError()

    wrapper.__name__ = func.__name__
    return wrapper


def build_app(
    path_prefix: str,
    title: str,
    api_version: str,
    summary: Optional[str] = None,
    description: Optional[str] = None,
    allow_origins: Optional[list[str]] = None,
) -> FastAPI:
    app = FastAPI(
        title=title,
        version=api_version,
        summary=summary or "",
        description=description or "",
        docs_url="/swagger",
        openapi_url="/openapi",
    )
    for handler in _HANDLERS_REGISTRY:
        method = getattr(app, handler.method.value.lower())
        method(
            f"{path_prefix}{handler.path}",
            name=handler.name,
            description=handler.description,
            response_model=handler.response_model,
            status_code=handler.status_code,
            tags=handler.tags,
            responses={
                error.status_code: {"model": error._to_pydantic_model()}
                for error in (handler.error_models or []) + [BaseApiError]
            },
        )(
            _default_handler_view_decorator(
                handler.view,
                handler.request_model,
                handler.response_model,
                handler.request_to_dto_mapping,
                handler.dto_to_response_mapping,
            )
        )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=allow_origins or [],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.exception_handler(BaseApiError)(_base_api_error_handler)
    return app
