from typing import Optional

from pydantic import BaseModel


class LocalStorageConfig(BaseModel):
    folder: str


class S3StorageConfig(BaseModel):
    endpoint_url: str
    aws_access_key_id: str
    aws_secret_access_key: str
    bucket: str


class FileStorageConfig(BaseModel):
    local: LocalStorageConfig
    s3: S3StorageConfig
    file_storage: str


_file_storage_config: Optional[FileStorageConfig] = None


def _init_file_storage_config(file_storage_config_value: FileStorageConfig):
    global _file_storage_config
    _file_storage_config = file_storage_config_value


def get_file_storage_config() -> FileStorageConfig:
    global _file_storage_config
    if _file_storage_config is None:
        raise ValueError("File storage config is not initialized.")
    return _file_storage_config
