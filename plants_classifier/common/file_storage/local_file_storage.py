import os
import uuid
from pathlib import Path
from typing import Optional

from .base_file_storage import IFileStorage
from .file_storage_config import get_file_storage_config


class LocalFileStorage(IFileStorage):
    def __init__(self):
        self.working_directory = (
            Path(__file__).parent.parent.parent.parent
            / get_file_storage_config().local.folder
        )
        if not os.path.isdir(self.working_directory):
            os.mkdir(self.working_directory)

    def save(self, data: bytes) -> str:
        file_id = str(uuid.uuid4())
        with open(self.working_directory / file_id, "wb") as f:
            f.write(data)

        return file_id

    def get(self, file_id: str) -> Optional[bytes]:
        try:
            with open(self.working_directory / file_id, "rb") as f:
                return f.read()
        except OSError:
            return None

    def delete(self, file_id: str) -> None:
        try:
            os.remove(self.working_directory / file_id)
        except OSError:
            return
