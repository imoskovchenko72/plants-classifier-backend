from abc import ABC, abstractmethod
from typing import Optional


class IFileStorage(ABC):
    """
    Very simplified file storage with only two actions:
    - save file with some unique id
    - get file by id
    """

    @abstractmethod
    def save(self, data: bytes) -> str:
        """
        Somehow saves file

        :param data: file content
        :return: id of saved file
        """
        pass

    @abstractmethod
    def get(self, file_id: str) -> Optional[bytes]:
        """
        Gives content of saved earlier file

        :param file_id:
        :return: bytes with file content if such file exists, None otherwise
        """
        pass

    @abstractmethod
    def delete(self, file_id: str) -> None:
        """
        Deletes file with specified id from storage

        :param file_id:
        :return:
        """
