import uuid
from typing import Optional

import boto3

from .base_file_storage import IFileStorage
from .file_storage_config import get_file_storage_config


class S3FileStorage(IFileStorage):
    def __init__(self):
        endpoint_url = get_file_storage_config().s3.endpoint_url
        aws_access_key_id = get_file_storage_config().s3.aws_access_key_id
        aws_secret_access_key = get_file_storage_config().s3.aws_secret_access_key
        self._client = boto3.client(
            "s3",
            endpoint_url=endpoint_url,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
        )
        self._bucket = get_file_storage_config().s3.bucket

    def save(self, data: bytes) -> str:
        file_id = str(uuid.uuid4())
        self._client.put_object(Bucket=self._bucket, Body=data, Key=file_id)

        return file_id

    def get(self, file_id: str) -> Optional[bytes]:
        return self._client.get_object(Bucket=self._bucket, Key=file_id)["Body"].read()

    def delete(self, file_id: str) -> None:
        self._client.delete_object(Bucket=self._bucket, Key=file_id)
