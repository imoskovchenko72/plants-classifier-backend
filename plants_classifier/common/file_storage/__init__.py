from .base_file_storage import IFileStorage
from .file_storage_config import FileStorageConfig, get_file_storage_config
from .local_file_storage import LocalFileStorage
from .s3_file_storage import S3FileStorage

_NAME_TO_STORAGE_TYPE_MAPPING = {"s3": S3FileStorage, "local": LocalFileStorage}
_DEFAULT_FILE_STORAGE_TYPE = LocalFileStorage


def get_file_storage() -> IFileStorage:
    return _NAME_TO_STORAGE_TYPE_MAPPING.get(
        get_file_storage_config().file_storage, _DEFAULT_FILE_STORAGE_TYPE
    )()
