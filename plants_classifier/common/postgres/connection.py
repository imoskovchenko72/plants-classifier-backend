import sqlalchemy

from .postgres_config import get_postgres_config
from .transaction_isolation_level import TransactionIsolationLevel


def _get_engine(isolation_level: TransactionIsolationLevel) -> sqlalchemy.Engine:
    return sqlalchemy.create_engine(
        url=get_postgres_url(), isolation_level=isolation_level.value
    )


def get_postgres_url() -> str:
    host = get_postgres_config().host
    port = get_postgres_config().port
    database = get_postgres_config().database

    user = get_postgres_config().user
    password = get_postgres_config().password

    return f"postgresql://{user}:{password}@{host}:{port}/{database}"
