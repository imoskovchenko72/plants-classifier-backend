from typing import Optional

from pydantic import BaseModel


class PostgresConfig(BaseModel):
    host: str
    port: int
    database: str
    user: str
    password: str


_postgres_config: Optional[PostgresConfig] = None


def _init_postgres_config(postgres_config_value: PostgresConfig):
    global _postgres_config
    _postgres_config = postgres_config_value


def get_postgres_config() -> PostgresConfig:
    global _postgres_config
    if _postgres_config is None:
        raise ValueError("Postgres config is not initialized.")
    return _postgres_config
