import functools
from typing import Any, Callable

from sqlalchemy.orm import Session

from .session import _get_session
from .transaction_isolation_level import TransactionIsolationLevel


def with_session(
    isolation_level: TransactionIsolationLevel = TransactionIsolationLevel.SERIALIZABLE,
) -> Callable:
    """
    Decorator which adds sqlalchemy session to kwargs.

    If session already passed to kwargs it checks that it is really sqlalchemy session and runs function as usual
    Otherwise it creates new session with specified transaction isolation level and adds
    session to 'db_session' kwarg then it calls function.
    If session was created on previous step then if function succeded then commit transaction othervise rollback

    Usage example:
    >>> @with_session
    >>> def func_with_session(some_string: str, db_session: Session):
    >>>     pass
    >>>
    >>> func_with_session("some important string")  # Not needed to pass session here, it will be added by decorator

    :param isolation_level: the isolation level that the transaction will be created with
    """

    def inner_decorator(func: Callable) -> Callable:
        @functools.wraps(func)
        def wrapped(*args, **kwargs) -> Any:
            if kwargs.get("db_session") is None:
                session = next(_get_session(isolation_level))
                kwargs.update({"db_session": session})
                try:
                    return_value = func(*args, **kwargs)
                except Exception:
                    session.rollback()
                    raise
                else:
                    session.commit()
                    return return_value
            else:
                if not isinstance(kwargs["db_session"], Session):
                    raise ValueError(
                        "kwarg 'db_session' should be sqlalchemy.orm.Session class or None"
                    )
                return func(*args, **kwargs)

        return wrapped

    return inner_decorator
