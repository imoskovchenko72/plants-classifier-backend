from typing import Generator

from sqlalchemy.orm import Session, sessionmaker

from .connection import _get_engine
from .transaction_isolation_level import TransactionIsolationLevel


def _get_session(
    isolation_level: TransactionIsolationLevel,
) -> Generator[Session, None, None]:
    session_maker = sessionmaker(_get_engine(isolation_level), expire_on_commit=True)
    with session_maker() as session:
        yield session
