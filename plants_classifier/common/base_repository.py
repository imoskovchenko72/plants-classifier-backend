import abc
from typing import Generic, Sequence, TypeVar

from sqlalchemy.orm import Session

from .postgres import with_session

Model = TypeVar("Model")


class IRepository(abc.ABC, Generic[Model]):
    @abc.abstractmethod
    def build(self, **kwargs) -> Model:
        """Creates Model object but not save into db"""
        pass

    @abc.abstractmethod
    @with_session()
    def create(self, db_session: Session, **kwargs) -> Model:
        """Creates Model object and saves into db"""
        pass

    @abc.abstractmethod
    @with_session()
    def save(self, model: Model, db_session: Session) -> None:
        """Saves Model object into db"""
        pass

    @abc.abstractmethod
    @with_session()
    def delete(self, model: Model, db_session: Session) -> None:
        """Deletes object from db"""
        pass

    @abc.abstractmethod
    @with_session()
    def all(self, db_session: Session) -> Sequence[Model]:
        """Gives all records from table"""
        pass
