from plants_classifier.core.settings import init_config

init_config()

from plants_classifier.core.enrichment import create_enriched_plant_model

if __name__ == "__main__":
    name = "Acer negundo"
    plant = create_enriched_plant_model(name).model_dump(mode="json")
    print(plant)
