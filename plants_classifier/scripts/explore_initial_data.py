from collections import defaultdict
from typing import Any

import yaml

from plants_classifier.core.settings import init_config

init_config()


def get_all_field_values(plants: list[dict[str, Any]], field_name: str) -> set[str]:
    values = set()
    for plant in plants:
        if isinstance(plant[field_name], list):
            values = values.union(plant[field_name])
        elif plant[field_name] is not None:
            values.add(plant[field_name])

    return values


def get_pairs_similarity(
    plants: list[dict[str, Any]], field_name: str
) -> dict[tuple[str, str], int]:
    values = defaultdict(int)
    for plant in plants:
        if isinstance(plant[field_name], list):
            for value1 in plant[field_name]:
                for value2 in plant[field_name]:
                    if value1 != value2:
                        values[(value1, value2)] += 1
    return dict(values)


def print_similarity_table(similarity: dict[tuple[str, str], int]):
    items_length = {}
    for key, value in similarity.items():
        if key[1] not in items_length:
            items_length[key[1]] = len(key[1])
        items_length[key[1]] = max(items_length[key[1]], len(str(value)))

    max_length = max(items_length.values())
    print(" " * max_length, end="")
    for key in items_length.keys():
        print("|", key, end="", sep="")
    print("|")

    for key1 in items_length.keys():
        print(key1 + " " * (max_length - len(key1)), end="")
        for key2 in items_length.keys():
            value = str(similarity[(key1, key2)]) if (key1, key2) in similarity else ""
            print("|", value + " " * (items_length[key2] - len(value)), end="", sep="")
        print("|")


if __name__ == "__main__":
    plants = []
    for i in range(26):
        with open(f"../data/initial_data_batch{i}.yaml", "r", encoding="utf-8") as file:
            plants += yaml.safe_load(file)

    print("leaf arrangement similarity:")
    print_similarity_table(get_pairs_similarity(plants, "leaf_arrangement"))

    print("life forms:", get_all_field_values(plants, "life_form"))
    print("shoots types:", get_all_field_values(plants, "shoots_type"))
    print("leaf arrangements:", get_all_field_values(plants, "leaf_arrangement"))
    print("leaf placements:", get_all_field_values(plants, "leaf_placement"))
    print("shoots surfaces:", get_all_field_values(plants, "shoots_surface"))
    print("appendages:", get_all_field_values(plants, "appendages"))
    print("leaf types:", get_all_field_values(plants, "leaf_type"))
    print("leaf blade shapes:", get_all_field_values(plants, "leaf_blade_shape"))
    print("leaf blade divisions:", get_all_field_values(plants, "leaf_blade_division"))
    print(
        "leaf blade complexity levels:",
        get_all_field_values(plants, "leaf_blade_complexity_level"),
    )
    print(
        "leaf blade attachments:", get_all_field_values(plants, "leaf_blade_attachment")
    )
    print("leaf parts shapes:", get_all_field_values(plants, "leaf_parts_shape"))
    print("leaf part divisions:", get_all_field_values(plants, "leaf_parts_division"))
    print("leaf apexes:", get_all_field_values(plants, "leaf_apex"))
    print("leaf margins:", get_all_field_values(plants, "leaf_margin"))
    print("leaf bases:", get_all_field_values(plants, "leaf_base"))
    print("leaf surfaces:", get_all_field_values(plants, "leaf_surface"))
    print("leaf appendages:", get_all_field_values(plants, "leaf_appendages"))
    print("inflorescense:", get_all_field_values(plants, "inflorescense"))
    print("flower main colours:", get_all_field_values(plants, "flower_main_colour"))
    print("flower colour tints:", get_all_field_values(plants, "flower_colour_tints"))
    print(
        "flowers spots and stripes:",
        get_all_field_values(plants, "flower_spots_and_stripes"),
    )
    print("flower sizes:", get_all_field_values(plants, "flower_size"))
    print("perianth:", get_all_field_values(plants, "perianth"))
    print("petals numbers:", get_all_field_values(plants, "petals_number"))
    print("fruit types:", get_all_field_values(plants, "fruits_type"))
    print("fruit colours:", get_all_field_values(plants, "fruit_colour"))
    print("fruit appendages:", get_all_field_values(plants, "fruit_appendages"))
    print("cones consistencies:", get_all_field_values(plants, "cones_consistency"))
    print("cones shapes:", get_all_field_values(plants, "cones_shape"))
    print("sporangias:", get_all_field_values(plants, "sporangia"))
