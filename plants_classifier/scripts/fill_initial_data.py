import yaml

from plants_classifier.core.settings import init_config

init_config()

from plants_classifier.common import AppLogger
from plants_classifier.core.enrichment import create_enriched_plant_model

logger = AppLogger(__name__)

BATCH_SIZE = 50


def process_batch(names: list[str], batch_number: int):
    logger.debug(f"Starting process bath #{batch_number}")

    models = []
    for name in names[
        batch_number * BATCH_SIZE : (batch_number + 1) * BATCH_SIZE  # noqa: E203
    ]:
        models.append(create_enriched_plant_model(name.strip()).model_dump(mode="json"))

    with open(
        f"../data/initial_data_batch{batch_number}.yaml", "w", encoding="utf-8"
    ) as output_data:
        yaml.dump(models, output_data, allow_unicode=True)

    logger.debug(f"Bath #{batch_number} processed")


if __name__ == "__main__":
    with open("../data/initial_plants_names.txt", "r", encoding="utf-8") as input_names:
        names = sorted(set(map(lambda s: s.lower().strip(), input_names.readlines())))

    start_batch = 1

    for i in range(start_batch * BATCH_SIZE, len(names), BATCH_SIZE):
        process_batch(names, i // BATCH_SIZE)
