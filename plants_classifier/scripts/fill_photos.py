import os.path
import pathlib
import zipfile

import sqlalchemy
from sqlalchemy.orm import sessionmaker

from plants_classifier.core.settings import init_config

init_config()

from plants_classifier.common import AppLogger
from plants_classifier.common.postgres import get_postgres_url
from plants_classifier.core.dto import PhotoWithData
from plants_classifier.core.repositories import PhotosRepository, PlantRepository
from plants_classifier.core.views import save_photo

logger = AppLogger(__name__)

ARCHIVE_NAME = "photos.zip"

if __name__ == "__main__":
    logger.info("Uploading photos from archive", archive_name=ARCHIVE_NAME)
    name_to_id = {}

    connection = sqlalchemy.create_engine(url=get_postgres_url())
    session_maker = sessionmaker(bind=connection)

    with session_maker() as session:
        photos = PhotosRepository().all(db_session=session)
        if len(photos) > 0:
            logger.info(
                "Photos table is not empty. Terminating script to avoid duplicates"
            )
            exit(0)
        plants = PlantRepository().all(db_session=session)
        for plant in plants:
            name_to_id[plant.name] = plant.id

        with zipfile.ZipFile(
            os.path.join(
                pathlib.Path(__file__).parent.resolve(),
                "..",
                "data",
                ARCHIVE_NAME,
            ),
            "r",
        ) as zip_ref:
            for filepath in zip_ref.namelist():
                filename = os.path.split(filepath)[1].lower()
                if not filename:
                    continue
                filename_without_format = filename.split(".")[0]
                plant_name = " ".join(filename_without_format.split()[:2])
                if plant_name not in name_to_id:
                    logger.warn("Plant not found for file", filepath=filepath)
                    continue
                plant_id = name_to_id[plant_name]

                content = zip_ref.read(filepath)
                save_photo(
                    PhotoWithData(data=content, filename=filename, plant_id=plant_id),
                    db_session=session,
                )

        session.commit()
