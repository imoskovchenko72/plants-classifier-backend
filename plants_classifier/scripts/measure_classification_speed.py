import functools
import time
from typing import Callable

from plants_classifier.core.settings import init_config

init_config()

from plants_classifier.core.classification import get_most_relevant_plants
from plants_classifier.core.dto import PlantCharacteristics


def with_time_measure(f: Callable):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        start_time = time.time()

        return_value = f(*args, **kwargs)

        end_time = time.time()
        print(f"Function {f.__name__} works {end_time - start_time}s")
        return return_value

    return inner


@with_time_measure
def classify(characteristics: PlantCharacteristics):
    return get_most_relevant_plants(
        characteristics=characteristics, taxonomy_filter=None
    )


if __name__ == "__main__":
    characteristics = PlantCharacteristics(
        life_form=["дерево листопадное"],
        leaf_arrangement=["супротивное"],
        leaf_type=["сложные"],
        leaf_blade_division=["перистое"],
    )
    print(classify(characteristics))
