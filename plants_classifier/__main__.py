import uvicorn

from plants_classifier.core.settings import init_config

init_config()

from plants_classifier.common.fastapi_application import build_app
from plants_classifier.core.settings import get_config
from plants_classifier.rest_api.endpoints import *  # noqa register all endpoints

if __name__ == "__main__":
    config = get_config()

    app = build_app(
        path_prefix=config.rest_api.path_prefix,
        title="Plants classifier",
        api_version=config.rest_api.version,
        allow_origins=config.rest_api.allow_origins,
    )
    uvicorn.run(
        app,
        host=config.web_server.host,
        port=config.web_server.port,
        log_level="info",
    )
