from decimal import Decimal
from typing import Optional

from fastapi import Body
from pydantic import Field

from .coordinates import SimpleCoordinatesRequest
from .filters import TaxonomyFilterRequest
from .pagination import BasePaginationRequest
from .plants import PlantCharacteristicsRequest


class GetPlantsByCharacteristicsRequest(
    BasePaginationRequest, TaxonomyFilterRequest, PlantCharacteristicsRequest
):
    latitude: Optional[Decimal] = Field(Body(default=None))
    longitude: Optional[Decimal] = Field(Body(default=None))


class GetTaxaByCharacteristicsRequest(
    BasePaginationRequest, PlantCharacteristicsRequest, SimpleCoordinatesRequest
):
    latitude: Optional[Decimal] = Field(Body(default=None))
    longitude: Optional[Decimal] = Field(Body(default=None))
