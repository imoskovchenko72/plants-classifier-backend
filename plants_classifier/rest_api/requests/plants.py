from typing import Optional

from fastapi import Body, Path
from pydantic import Field

from plants_classifier.common import BaseRequest

from .filters import TaxonomyFilterRequest
from .pagination import BasePaginationRequest


class GetPlantRequest(BaseRequest):
    id: int = Field(Path())


class DeletePlantRequest(BaseRequest):
    id: int = Field(Path())


class PlantCharacteristicsRequest(BaseRequest):
    life_form: Optional[list[str]] = Field(Body(default=None))  # жизненная форма

    shoots_type: Optional[list[str]] = Field(Body(default=None))  # тип побегов
    leaf_arrangement: Optional[list[str]] = Field(
        Body(default=None)
    )  # листорасположение
    leaf_placement: Optional[list[str]] = Field(
        Body(default=None)
    )  # размещение листьев
    shoots_surface: Optional[list[str]] = Field(
        Body(default=None)
    )  # поверхность побегов
    appendages: Optional[list[str]] = Field(Body(default=None))  # придатки

    leaf_type: Optional[list[str]] = Field(Body(default=None))  # тип листьев
    leaf_blade_shape: Optional[list[str]] = Field(
        Body(default=None)
    )  # форма листовой пластинки
    leaf_blade_division: Optional[list[str]] = Field(
        Body(default=None)
    )  # членение листовой пластинки
    leaf_blade_complexity_level: Optional[list[str]] = Field(
        Body(default=None)
    )  # порядок сложности листовой пластинки
    leaf_blade_attachment: Optional[list[str]] = Field(
        Body(default=None)
    )  # прикрепление листовой пластинки
    leaf_parts_shape: Optional[list[str]] = Field(
        Body(default=None)
    )  # форма долей листьев
    leaf_parts_division: Optional[list[str]] = Field(
        Body(default=None)
    )  # членение долей листьев
    leaf_apex: Optional[list[str]] = Field(Body(default=None))  # верхушка листа
    leaf_margin: Optional[list[str]] = Field(Body(default=None))  # край листа
    leaf_base: Optional[list[str]] = Field(Body(default=None))  # основание листа
    leaf_surface: Optional[list[str]] = Field(Body(default=None))  # поверхность листа
    leaf_appendages: Optional[list[str]] = Field(Body(default=None))  # придатки листа

    inflorescense: Optional[list[str]] = Field(Body(default=None))  # соцветие
    flower_main_colour: Optional[list[str]] = Field(
        Body(default=None)
    )  # основной цвет цветка
    flower_colour_tints: Optional[list[str]] = Field(
        Body(default=None)
    )  # оттенки цветка
    flower_spots_and_stripes: Optional[list[str]] = Field(
        Body(default=None)
    )  # пятна и полоски цветка
    flower_size: Optional[list[str]] = Field(Body(default=None))  # размер цветка
    perianth: Optional[list[str]] = Field(Body(default=None))  # околоцветник
    petals_number: Optional[list[str]] = Field(Body(default=None))  # число лепестков

    fruits_type: Optional[list[str]] = Field(Body(default=None))  # тип плодов
    fruit_colour: Optional[list[str]] = Field(Body(default=None))  # окраска плодов
    fruit_appendages: Optional[list[str]] = Field(Body(default=None))  # придатки плодов

    cones_consistency: Optional[list[str]] = Field(
        Body(default=None)
    )  # консистенция шишек
    cones_shape: Optional[list[str]] = Field(Body(default=None))  # форма шишек

    sporangia: Optional[list[str]] = Field(Body(default=None))  # спорангии


class PlantRequest(PlantCharacteristicsRequest):
    name: str = Field(Body())  # латинское название

    russian_names: Optional[list[str]] = Field(Body(default=None))  # русские названия
    is_endangered: bool = Field((Body(default=False)))
    description: Optional[str] = Field(
        Body(default=None)
    )  # некоторое дополнительное описание

    division: Optional[str] = Field(Body(default=None))  # отдел
    plant_class: Optional[str] = Field(Body(default=None))  # класс
    ordo: Optional[str] = Field(Body(default=None))  # порядок
    familia: Optional[str] = Field(Body(default=None))  # семейство
    genus: Optional[str] = Field(Body(default=None))  # род
    species: Optional[str] = Field(Body(default=None))  # вид


class UpdatePlantRequest(PlantRequest):
    id: int = Field(Path())


class GetPlantByTaxonomyRequest(BasePaginationRequest, TaxonomyFilterRequest):
    pass
