from typing import Optional

from fastapi import Body, File, Path, UploadFile
from pydantic import Field

from plants_classifier.common import BaseRequest


class GetPlantPhotosRequest(BaseRequest):
    id: int = Field(Path())


class GetPhotoRequest(BaseRequest):
    id: str = Field(Path())


class DeletePhotoRequest(BaseRequest):
    id: str = Field(Path())


class SavePhotoRequest(BaseRequest):
    description: Optional[str] = Field(Body(default=None))
    plant_id: int = Field(Body())
    file: UploadFile = Field(File)
