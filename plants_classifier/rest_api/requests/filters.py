from typing import Optional

from fastapi import Query
from pydantic import Field

from plants_classifier.common import BaseRequest


class TaxonomyFilterRequest(BaseRequest):
    taxon_id: Optional[int] = Field(Query(default=None))
