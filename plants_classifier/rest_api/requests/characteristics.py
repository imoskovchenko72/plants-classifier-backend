from fastapi import Query
from pydantic import Field

from plants_classifier.common import BaseRequest
from plants_classifier.core.constants import PlantCharacteristicType


class GetCharacteristicsByTypeRequest(BaseRequest):
    type: PlantCharacteristicType = Field(Query())
