from typing import Optional

from fastapi import Query
from pydantic import Field

from plants_classifier.core.constants import TaxonType

from .filters import TaxonomyFilterRequest
from .pagination import BasePaginationRequest


class GetPlantsByQueryRequest(BasePaginationRequest, TaxonomyFilterRequest):
    query: str = Field(Query())


class GetTaxaByQueryRequest(BasePaginationRequest):
    query: str = Field(Query())
    taxon_type: Optional[TaxonType] = Field(Query(default=None))
