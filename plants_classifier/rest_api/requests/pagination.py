from fastapi import Query
from pydantic import Field

from plants_classifier.common import BaseRequest
from plants_classifier.core.constants import UNLIMITED


class BasePaginationRequest(BaseRequest):
    limit: int = Field(
        Query(
            default=UNLIMITED,
            description=f"Max number of elements to return (maybe less). {UNLIMITED} means all possible",
        )
    )
    offset: int = Field(Query(default=0, description="Number of elements to skip"))
