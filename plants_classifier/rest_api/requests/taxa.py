from typing import Optional

from fastapi import Body, Path
from pydantic import Field

from plants_classifier.common import BaseRequest
from plants_classifier.core.constants import TaxonType


class GetTaxonRequest(BaseRequest):
    id: int = Field(Path())


class DeleteTaxonRequest(BaseRequest):
    id: int = Field(Path())


class TaxonRequest(BaseRequest):
    name: str = Field(Body())
    type: TaxonType = Field(Body())
    description: Optional[str] = Field(Body(default=None))
    russian_names: Optional[list[str]] = Field(Body(default=None))


class UpdateTaxonRequest(TaxonRequest):
    id: int = Field(Path())
