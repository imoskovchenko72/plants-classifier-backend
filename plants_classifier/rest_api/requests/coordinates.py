import datetime
from decimal import Decimal
from typing import Optional

from fastapi import Body, Path
from pydantic import Field

from plants_classifier.common import BaseRequest


class GetPlantCoordinatesRequest(BaseRequest):
    id: int = Field(Path())


class DeleteCoordinatesRequest(BaseRequest):
    id: int = Field(Path())


class SimpleCoordinatesRequest(BaseRequest):
    latitude: Decimal = Field(Body())
    longitude: Decimal = Field(Body())


class SaveCoordinatesRequest(SimpleCoordinatesRequest):
    description: Optional[str] = Field(Body(default=None))
    plant_id: int = Field(Body())
    date: Optional[datetime.date] = Field(Body(default_factory=datetime.date.today))
