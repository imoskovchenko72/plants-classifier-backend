from http import HTTPMethod, HTTPStatus

from fastapi import Response

from plants_classifier.common import Handler
from plants_classifier.core.dto import PhotoWithData
from plants_classifier.core.views import (
    delete_photo,
    get_all_plant_photos,
    get_photo,
    get_photo_miniature,
    save_photo,
)

from ..requests import (
    DeletePhotoRequest,
    GetPhotoRequest,
    GetPlantPhotosRequest,
    SavePhotoRequest,
)
from ..responses import PhotoListResponse, PhotoResponse

Handler(
    path="/photos/upload",
    method=HTTPMethod.POST,
    view=save_photo,
    request_model=SavePhotoRequest,
    response_model=PhotoResponse,
    status_code=HTTPStatus.CREATED,
    tags=["photos"],
    request_to_dto_mapping=lambda r: PhotoWithData(
        data=r.file.file.read(),
        description=r.description,
        filename=r.file.filename,
        plant_id=r.plant_id,
    ),
)

Handler(
    path="/photos/delete/{id}",
    method=HTTPMethod.DELETE,
    view=delete_photo,
    request_model=DeletePhotoRequest,
    response_model=None,
    status_code=HTTPStatus.NO_CONTENT,
    tags=["photos"],
    request_to_dto_mapping=lambda r: r.id,
)

Handler(
    path="/photos/download/{id}",
    method=HTTPMethod.GET,
    view=get_photo,
    request_model=GetPhotoRequest,
    response_model=None,  # fastapi response used here
    status_code=HTTPStatus.OK,
    tags=["photos"],
    request_to_dto_mapping=lambda r: r.id,
    dto_to_response_mapping=lambda r: Response(content=r, media_type="image/png"),
)


Handler(
    path="/photos/download/{id}/mini",
    method=HTTPMethod.GET,
    view=get_photo_miniature,
    request_model=GetPhotoRequest,
    response_model=None,  # fastapi response used here
    status_code=HTTPStatus.OK,
    tags=["photos"],
    request_to_dto_mapping=lambda r: r.id,
    dto_to_response_mapping=lambda r: Response(content=r, media_type="image/png"),
)


Handler(
    path="/plants/{id}/photos",
    method=HTTPMethod.GET,
    view=get_all_plant_photos,
    request_model=GetPlantPhotosRequest,
    response_model=PhotoListResponse,
    status_code=HTTPStatus.OK,
    tags=["plant"],
    request_to_dto_mapping=lambda r: r.id,
    dto_to_response_mapping=lambda r: PhotoListResponse(
        photos=[p.model_dump(mode="json") for p in r]
    ),
)
