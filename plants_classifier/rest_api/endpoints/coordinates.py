from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.views import (
    delete_coordinates,
    get_all_plant_coordinates,
    save_coordinates,
)

from ..requests import (
    DeleteCoordinatesRequest,
    GetPlantCoordinatesRequest,
    SaveCoordinatesRequest,
)
from ..responses import CoordinatesListResponse, CoordinatesResponse

Handler(
    path="/coordinates/create",
    method=HTTPMethod.POST,
    view=save_coordinates,
    request_model=SaveCoordinatesRequest,
    response_model=CoordinatesResponse,
    status_code=HTTPStatus.CREATED,
    tags=["coordinates"],
)


Handler(
    path="/coordinates/{id}/delete",
    method=HTTPMethod.DELETE,
    view=delete_coordinates,
    request_model=DeleteCoordinatesRequest,
    response_model=None,
    status_code=HTTPStatus.NO_CONTENT,
    request_to_dto_mapping=lambda r: r.id,
    tags=["coordinates"],
)


Handler(
    path="/plants/{id}/coordinates",
    method=HTTPMethod.GET,
    view=get_all_plant_coordinates,
    request_model=GetPlantCoordinatesRequest,
    response_model=CoordinatesListResponse,
    status_code=HTTPStatus.OK,
    tags=["plant"],
    request_to_dto_mapping=lambda r: r.id,
    dto_to_response_mapping=lambda r: CoordinatesListResponse(
        coordinates=[p.model_dump(mode="json") for p in r]
    ),
)
