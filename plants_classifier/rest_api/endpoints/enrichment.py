from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.views import enrich_plant

from ..requests import PlantRequest
from ..responses import EnrichedPlantResponse

Handler(
    path="/plants/enrich",
    method=HTTPMethod.PATCH,
    view=enrich_plant,
    request_model=PlantRequest,
    response_model=EnrichedPlantResponse,
    status_code=HTTPStatus.OK,
    tags=["enrichment"],
    description="Fills some missed plant info using data from open sources",
)
