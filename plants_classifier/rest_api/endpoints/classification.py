from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.dto import (
    ClassifyPlantRequest,
    ClassifyTaxonRequest,
    PlantCharacteristics,
    SimpleCoordinates,
)
from plants_classifier.core.dto.taxon import TaxonomyFilter
from plants_classifier.core.views import (
    get_plants_by_characteristics,
    get_taxa_by_characteristics,
)

from ..requests import (
    GetPlantsByCharacteristicsRequest,
    GetTaxaByCharacteristicsRequest,
)
from ..responses import (
    ShortPlantInfoWithRelevanceListResponse,
    TaxonWithRelevanceListResponse,
)

Handler(
    path="/classify/plant",
    method=HTTPMethod.PATCH,
    view=get_plants_by_characteristics,
    request_model=GetPlantsByCharacteristicsRequest,
    response_model=ShortPlantInfoWithRelevanceListResponse,
    status_code=HTTPStatus.OK,
    tags=["classification"],
    request_to_dto_mapping=lambda r: ClassifyPlantRequest(
        taxonomy=TaxonomyFilter(**r.model_dump(mode="json"), ignore_extra_fields=True),
        characteristics=PlantCharacteristics(
            **r.model_dump(mode="json"), ignore_extra_fields=True
        ),
        coordinates=SimpleCoordinates(
            **r.model_dump(mode="json"), ignore_extra_fields=True
        )
        if r.latitude is not None and r.longitude is not None
        else None,
        **r.model_dump(mode="json"),
        ignore_extra_fields=True,
    ),
    dto_to_response_mapping=lambda r: ShortPlantInfoWithRelevanceListResponse(
        plants=[p.model_dump(mode="json") for p in r]
    ),
)


Handler(
    path="/classify/taxon",
    method=HTTPMethod.PATCH,
    view=get_taxa_by_characteristics,
    request_model=GetTaxaByCharacteristicsRequest,
    response_model=TaxonWithRelevanceListResponse,
    status_code=HTTPStatus.OK,
    tags=["classification"],
    request_to_dto_mapping=lambda r: ClassifyTaxonRequest(
        characteristics=PlantCharacteristics(
            **r.model_dump(mode="json"), ignore_extra_fields=True
        ),
        coordinates=SimpleCoordinates(
            **r.model_dump(mode="json"), ignore_extra_fields=True
        )
        if r.latitude is not None and r.longitude is not None
        else None,
        **r.model_dump(mode="json"),
        ignore_extra_fields=True,
    ),
    dto_to_response_mapping=lambda r: TaxonWithRelevanceListResponse(
        taxa=[t.model_dump(mode="json") for t in r]
    ),
)
