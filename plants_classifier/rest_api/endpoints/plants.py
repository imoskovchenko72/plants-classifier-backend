from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.dto import PlantByTaxonomyRequest
from plants_classifier.core.dto.taxon import TaxonomyFilter
from plants_classifier.core.exceptions import (
    PlantAlreadyExistsError,
    PlantNotFoundError,
)
from plants_classifier.core.views import (
    delete_plant,
    get_plant,
    get_plants_by_taxonomy,
    save_plant,
    update_plant,
)

from ..requests import (
    DeletePlantRequest,
    GetPlantByTaxonomyRequest,
    GetPlantRequest,
    PlantRequest,
    UpdatePlantRequest,
)
from ..responses import PlantResponse, ShortPlantInfoListResponse

Handler(
    path="/plants/{id}/get",
    method=HTTPMethod.GET,
    view=get_plant,
    request_model=GetPlantRequest,
    response_model=PlantResponse,
    status_code=HTTPStatus.OK,
    error_models=[PlantNotFoundError],
    tags=["plant"],
    request_to_dto_mapping=lambda r: r.id,
)


Handler(
    path="/plants/create",
    method=HTTPMethod.POST,
    view=save_plant,
    request_model=PlantRequest,
    response_model=PlantResponse,
    status_code=HTTPStatus.CREATED,
    error_models=[PlantAlreadyExistsError],
    tags=["plant"],
)


Handler(
    path="/plants/{id}/delete",
    method=HTTPMethod.DELETE,
    view=delete_plant,
    request_model=DeletePlantRequest,
    response_model=None,
    status_code=HTTPStatus.NO_CONTENT,
    tags=["plant"],
    request_to_dto_mapping=lambda r: r.id,
)


Handler(
    path="/plants/{id}/update",
    method=HTTPMethod.PUT,
    view=update_plant,
    request_model=UpdatePlantRequest,
    response_model=PlantResponse,
    status_code=HTTPStatus.OK,
    error_models=[PlantNotFoundError, PlantAlreadyExistsError],
    tags=["plant"],
)


Handler(
    path="/plants/get",
    method=HTTPMethod.GET,
    view=get_plants_by_taxonomy,
    request_model=GetPlantByTaxonomyRequest,
    response_model=ShortPlantInfoListResponse,
    status_code=HTTPStatus.OK,
    tags=["plant"],
    request_to_dto_mapping=lambda r: PlantByTaxonomyRequest(
        taxonomy=TaxonomyFilter(**r.model_dump(mode="json"), ignore_extra_fields=True),
        **r.model_dump(mode="json"),
        ignore_extra_fields=True,
    ),
    dto_to_response_mapping=lambda r: ShortPlantInfoListResponse(
        plants=[p.model_dump(mode="json") for p in r]
    ),
)
