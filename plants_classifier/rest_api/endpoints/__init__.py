from .characteristics import *  # noqa: requierd to register all handlers
from .classification import *  # noqa: requierd to register all handlers
from .coordinates import *  # noqa: requierd to register all handlers
from .enrichment import *  # noqa: requierd to register all handlers
from .photos import *  # noqa: requierd to register all handlers
from .plants import *  # noqa: requierd to register all handlers
from .search import *  # noqa: requierd to register all handlers
from .taxa import *  # noqa: requierd to register all handlers
