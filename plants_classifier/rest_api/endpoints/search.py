from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.dto import SearchPlantRequest
from plants_classifier.core.dto.taxon import TaxonomyFilter
from plants_classifier.core.views import (
    get_plants_by_search_query,
    get_taxa_by_search_query,
)

from ..requests import GetPlantsByQueryRequest, GetTaxaByQueryRequest
from ..responses import ShortPlantInfoListResponse, TaxonListResponse

Handler(
    path="/search/plant",
    method=HTTPMethod.GET,
    view=get_plants_by_search_query,
    request_model=GetPlantsByQueryRequest,
    response_model=ShortPlantInfoListResponse,
    status_code=HTTPStatus.OK,
    tags=["search"],
    request_to_dto_mapping=lambda r: SearchPlantRequest(
        taxonomy=TaxonomyFilter(**r.model_dump(mode="json"), ignore_extra_fields=True),
        **r.model_dump(mode="json"),
        ignore_extra_fields=True,
    ),
    dto_to_response_mapping=lambda r: ShortPlantInfoListResponse(
        plants=[p.model_dump(mode="json") for p in r]
    ),
)


Handler(
    path="/search/taxon",
    method=HTTPMethod.GET,
    view=get_taxa_by_search_query,
    request_model=GetTaxaByQueryRequest,
    response_model=TaxonListResponse,
    status_code=HTTPStatus.OK,
    tags=["search"],
    dto_to_response_mapping=lambda r: TaxonListResponse(
        taxa=[p.model_dump(mode="json") for p in r]
    ),
)
