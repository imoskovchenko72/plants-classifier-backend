from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.views import get_all_characteristics_by_type

from ..requests import GetCharacteristicsByTypeRequest
from ..responses import CharacteristicsResponse

Handler(
    path="/characteristics/get",
    method=HTTPMethod.GET,
    view=get_all_characteristics_by_type,
    request_model=GetCharacteristicsByTypeRequest,
    response_model=CharacteristicsResponse,
    status_code=HTTPStatus.OK,
    tags=["characteristics"],
    request_to_dto_mapping=lambda r: r.type,
    dto_to_response_mapping=lambda r: CharacteristicsResponse(characteristics=r),
)
