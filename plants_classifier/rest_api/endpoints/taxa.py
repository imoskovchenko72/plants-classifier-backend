from http import HTTPMethod, HTTPStatus

from plants_classifier.common import Handler
from plants_classifier.core.exceptions import (
    TaxonAlreadyExistsError,
    TaxonNotFoundError,
)
from plants_classifier.core.views import (
    create_taxon,
    delete_taxon,
    get_taxon,
    update_taxon,
)

from ..requests import (
    DeleteTaxonRequest,
    GetTaxonRequest,
    TaxonRequest,
    UpdateTaxonRequest,
)
from ..responses import TaxonResponse

Handler(
    path="/taxa/{id}/get",
    method=HTTPMethod.GET,
    view=get_taxon,
    request_model=GetTaxonRequest,
    response_model=TaxonResponse,
    status_code=HTTPStatus.OK,
    error_models=[TaxonNotFoundError],
    tags=["taxon"],
    request_to_dto_mapping=lambda r: r.id,
)


Handler(
    path="/taxa/create",
    method=HTTPMethod.POST,
    view=create_taxon,
    request_model=TaxonRequest,
    response_model=TaxonResponse,
    status_code=HTTPStatus.CREATED,
    error_models=[TaxonAlreadyExistsError],
    tags=["taxon"],
)


Handler(
    path="/taxa/{id}/delete",
    method=HTTPMethod.DELETE,
    view=delete_taxon,
    request_model=DeleteTaxonRequest,
    response_model=None,
    status_code=HTTPStatus.NO_CONTENT,
    tags=["taxon"],
    request_to_dto_mapping=lambda r: r.id,
)


Handler(
    path="/taxa/{id}/update",
    method=HTTPMethod.PUT,
    view=update_taxon,
    request_model=UpdateTaxonRequest,
    response_model=TaxonResponse,
    status_code=HTTPStatus.OK,
    error_models=[TaxonNotFoundError, TaxonAlreadyExistsError],
    tags=["taxon"],
)
