from typing import Optional

from plants_classifier.common import BaseResponse

from .relevance import WithRelevanceResponseMixin
from .taxa import ShortTaxonInfoResponse


class PlantCharacteristicsResponse(BaseResponse):
    name: str  # латинское название

    is_endangered: bool = False
    russian_names: Optional[list[str]] = None  # русские названия
    description: Optional[str] = None  # некоторое дополнительное описание

    life_form: Optional[list[str]] = None  # жизненная форма

    shoots_type: Optional[list[str]] = None  # тип побегов
    leaf_arrangement: Optional[list[str]] = None  # листорасположение
    leaf_placement: Optional[list[str]] = None  # размещение листьев
    shoots_surface: Optional[list[str]] = None  # поверхность побегов
    appendages: Optional[list[str]] = None  # придатки

    leaf_type: Optional[list[str]] = None  # тип листьев
    leaf_blade_shape: Optional[list[str]] = None  # форма листовой пластинки
    leaf_blade_division: Optional[list[str]] = None  # членение листовой пластинки
    leaf_blade_complexity_level: Optional[
        list[str]
    ] = None  # порядок сложности листовой пластинки
    leaf_blade_attachment: Optional[list[str]] = None  # прикрепление листовой пластинки
    leaf_parts_shape: Optional[list[str]] = None  # форма долей листьев
    leaf_parts_division: Optional[list[str]] = None  # членение долей листьев
    leaf_apex: Optional[list[str]] = None  # верхушка листа
    leaf_margin: Optional[list[str]] = None  # край листа
    leaf_base: Optional[list[str]] = None  # основание листа
    leaf_surface: Optional[list[str]] = None  # поверхность листа
    leaf_appendages: Optional[list[str]] = None  # придатки листа

    inflorescense: Optional[list[str]] = None  # соцветие
    flower_main_colour: Optional[list[str]] = None  # основной цвет цветка
    flower_colour_tints: Optional[list[str]] = None  # оттенки цветка
    flower_spots_and_stripes: Optional[list[str]] = None  # пятна и полоски цветка
    flower_size: Optional[list[str]] = None  # размер цветка
    perianth: Optional[list[str]] = None  # околоцветник
    petals_number: Optional[list[str]] = None  # число лепестков

    fruits_type: Optional[list[str]] = None  # тип плодов
    fruit_colour: Optional[list[str]] = None  # окраска плодов
    fruit_appendages: Optional[list[str]] = None  # придатки плодов

    cones_consistency: Optional[list[str]] = None  # консистенция шишек
    cones_shape: Optional[list[str]] = None  # форма шишек

    sporangia: Optional[list[str]] = None  # спорангии


class EnrichedPlantResponse(PlantCharacteristicsResponse):
    """
    Describes plant not saved into db
    """

    division: Optional[str] = None  # отдел
    plant_class: Optional[str] = None  # класс
    ordo: Optional[str] = None  # порядок
    familia: Optional[str] = None  # семейство
    genus: Optional[str] = None  # род
    species: Optional[str] = None  # вид


class PlantResponse(PlantCharacteristicsResponse):
    id: int

    division: Optional[ShortTaxonInfoResponse] = None  # отдел
    plant_class: Optional[ShortTaxonInfoResponse] = None  # класс
    ordo: Optional[ShortTaxonInfoResponse] = None  # порядок
    familia: Optional[ShortTaxonInfoResponse] = None  # семейство
    genus: Optional[ShortTaxonInfoResponse] = None  # род
    species: Optional[ShortTaxonInfoResponse] = None  # вид


class ShortPlantInfoResponse(BaseResponse):
    id: int
    name: str
    is_endangered: bool = False
    russian_name: Optional[str] = None
    photo_id: Optional[str] = None


class ShortPlantInfoListResponse(BaseResponse):
    plants: list[ShortPlantInfoResponse]


class ShortPlantInfoWithRelevanceResponse(
    ShortPlantInfoResponse, WithRelevanceResponseMixin
):
    pass


class ShortPlantInfoWithRelevanceListResponse(BaseResponse):
    plants: list[ShortPlantInfoWithRelevanceResponse]
