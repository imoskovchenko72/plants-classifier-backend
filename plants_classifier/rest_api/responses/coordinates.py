import datetime
from decimal import Decimal
from typing import Optional

from plants_classifier.common import BaseResponse


class CoordinatesResponse(BaseResponse):
    id: int
    description: Optional[str] = None
    plant_id: int
    date: Optional[datetime.date] = None

    latitude: Decimal
    longitude: Decimal


class CoordinatesListResponse(BaseResponse):
    coordinates: list[CoordinatesResponse]
