from typing import Optional

from plants_classifier.common import BaseResponse


class PhotoResponse(BaseResponse):
    id: str
    description: Optional[str] = None
    filename: Optional[str] = None
    plant_id: int


class PhotoListResponse(BaseResponse):
    photos: list[PhotoResponse]
