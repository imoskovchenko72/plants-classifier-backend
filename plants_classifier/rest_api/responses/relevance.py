from plants_classifier.common import BaseResponse


class WithRelevanceResponseMixin(BaseResponse):
    relevance_ratio: float
