from typing import Optional

from plants_classifier.common import BaseResponse
from plants_classifier.core.constants import TaxonType

from .relevance import WithRelevanceResponseMixin


class TaxonResponse(BaseResponse):
    id: int
    name: str
    type: TaxonType
    description: Optional[str] = None
    russian_names: Optional[list[str]] = None


class ShortTaxonInfoResponse(BaseResponse):
    id: int
    name: str


class TaxonListResponse(BaseResponse):
    taxa: list[TaxonResponse]


class TaxonWithRelevanceResponse(TaxonResponse, WithRelevanceResponseMixin):
    pass


class TaxonWithRelevanceListResponse(BaseResponse):
    taxa: list[TaxonWithRelevanceResponse]
