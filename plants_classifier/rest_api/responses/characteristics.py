from plants_classifier.common import BaseResponse


class CharacteristicsResponse(BaseResponse):
    characteristics: list[str]
