import json
import traceback
from typing import Optional

import requests
from bs4 import BeautifulSoup

from plants_classifier.common import AppLogger
from plants_classifier.core.dto import Plant
from plants_classifier.core.enrichment.enrichers.base_enricher import IEnricher
from plants_classifier.core.settings import get_config

logger = AppLogger(__name__)


class PlantariumEnricher(IEnricher):
    _PLANTARIUM_URL = "https://www.plantarium.ru"
    _CONNECTION_TIMEOUT = 10
    _MAX_CONNECTION_RETRIES = 5

    def enrich(self, name: str, data: Plant, allow_override_data: bool = False):
        logger.debug(
            "Plantarium enricher started to enrich plant model", plant_name=name
        )

        link = self._get_most_relevant_link(name)
        plant_page_content = self._get_http_response(link)

        parsed_page = BeautifulSoup(plant_page_content, features="html.parser")

        if allow_override_data or data.russian_names is None:
            data.russian_names = self._get_russian_names(parsed_page)

        for key, value in self._get_taxonomy(parsed_page).items():
            if allow_override_data or not data.__dict__[key]:
                data.__dict__[key] = value

        for key, value in self._get_characteristics(parsed_page).items():
            if allow_override_data or not data.__dict__[key]:
                data.__dict__[key] = value

        logger.debug(
            "Plantarium enricher successfully enriched plant model", plant_name=name
        )

    def _get_most_relevant_link(self, name: str) -> str:
        """
        Finds page with most relevant plant description.

        plantarium search request looks like:
        https://www.plantarium.ru/opensearch-suggest.php?jsonp=<any-str1>&q=<request>&_=<any-str2>

        plantarium serach response looks like:
        <any-str1>([<request>, [<name1>, <name2>, ...], [], [<link to plant with name1>, ...]])

        :param name: plant name
        :return: link to plant page with most similar name
        """
        search_response = self._get_http_response(
            f"{self._PLANTARIUM_URL}/opensearch-suggest.php?jsonp=1&_=2&q={name.lower().replace(' ', '+')}"
        )

        try:
            data = json.loads(
                search_response[2:-1]
            )  # first two symbols are '1(' last symbol is ')'
            for i, found_name in enumerate(data[1]):
                if name.lower() == found_name.lower():
                    return data[3][i]

            logger.warn("The exact match of plant name not found", plant_name=name)
            return data[3][
                0
            ]  # we hope first link from plantarium answer is most relevant
        except Exception:
            logger.error(
                "Something went wrong during parsing plantarium response",
                plant_name=name,
                response=search_response,
                traceback=traceback.format_exc(),
            )
            raise

    @staticmethod
    def _get_russian_names(page: BeautifulSoup) -> list[str]:
        names = []
        for name in page.find_all(attrs={"class": "name-list-item"}):
            name_span = name.find_all("span")[0]
            names.append(name_span.text.lower())
        return names

    @staticmethod
    def _get_taxonomy(page: BeautifulSoup) -> dict[str, str]:
        taxon_types_mapping = {
            "отдел": "division",
            "класс": "plant_class",
            "порядок": "ordo",
            "семейство": "familia",
            "род": "genus",
            "вид": "species",
        }
        taxonomy = {}
        taxon_table = page.find_all("table", attrs={"class": "taxon-path"})[0]
        for row in taxon_table.find_all("tr"):
            taxon_type = row.find_all("td", attrs={"class": "taxon-type"})[
                0
            ].text.lower()
            taxon_name = row.find_all(attrs={"class": "taxon-name"})[0].text.lower()
            if taxon_type in taxon_types_mapping:
                taxonomy[taxon_types_mapping[taxon_type]] = taxon_name
        return taxonomy

    def _get_characteristics(self, page: BeautifulSoup) -> dict[str, list[str]]:
        tree = page.find_all(attrs={"id": "trainsSectionContent"})[0].find_all(
            attrs={"class": "tree"}
        )[0]
        data: dict[str, list[str]] = {}
        self.__fill_data_recursively(tree, data, [])
        return data

    def __fill_data_recursively(
        self, node: BeautifulSoup, data: dict[str, list[str]], path: list[str]
    ):
        is_node_terminal = True
        was_node_name_added = False

        # find node name and check is node terminal
        for children in node.findChildren("div", recursive=False):
            if children["class"] == ["tree-node-item"]:
                node_name = children.find_all("span")[0].text.lower()
                path.append(node_name)
                was_node_name_added = True
            else:
                is_node_terminal = False

        # go to child nodes
        for children in node.findChildren("div", recursive=False):
            if children["class"] != ["tree-node-item"]:
                self.__fill_data_recursively(children, data, path)

        if is_node_terminal:
            mapping = self.__find_path_mapping(path)
            if mapping is not None:
                self._add_key_value_pair_to_data(data, mapping[0], mapping[1])

        if was_node_name_added:
            path.pop()

    @staticmethod
    def __find_path_mapping(real_path: list[str]) -> Optional[tuple[str, str]]:
        for (
            maybe_path,
            maybe_converted_path,
        ) in get_config().plantarium.path_mappings.items():
            variables = {}
            i = 0
            is_path_correct = True
            for path_item in maybe_path.split("/"):
                if i == len(real_path):
                    is_path_correct = False
                    break

                if path_item == "*":
                    variables["*"] = " ".join(real_path[i:])
                    break
                elif path_item.startswith("$"):
                    variables[path_item] = real_path[i]
                    i += 1
                else:
                    if real_path[i].lower() == path_item.lower():
                        i += 1
                    else:
                        is_path_correct = False
                        break

            if is_path_correct:
                if maybe_converted_path is None:
                    return None
                path_key, path_value = maybe_converted_path.split("/")
                path_key = path_key.lower()
                path_value = path_value.lower()

                for variable_key, variable_value in variables.items():
                    path_value = path_value.replace(variable_key, variable_value)
                return path_key, path_value

        logger.warn("Mapping not found for some path", path="/".join(real_path))

    @staticmethod
    def _add_key_value_pair_to_data(data: dict[str, list[str]], key: str, value: str):
        if key not in data:
            data[key] = []
        data[key].append(value)

    def _get_http_response(self, request_path: str) -> bytes:
        # connection with plantarium is unstable. Some retries with small timeout helps.
        for i in range(self._MAX_CONNECTION_RETRIES):
            try:
                return requests.get(
                    request_path, timeout=self._CONNECTION_TIMEOUT
                ).content
            except requests.exceptions.ConnectTimeout:
                logger.warn(
                    f"Connection timed out. Attempt {i + 1}", request=request_path
                )
            except Exception:
                logger.error(
                    "error occurred during handling request",
                    request=request_path,
                    traceback=traceback.format_exc(),
                )
                raise

        logger.error("Connections limit exceeded. Giving up", request=request_path)
        raise requests.exceptions.ConnectTimeout()
