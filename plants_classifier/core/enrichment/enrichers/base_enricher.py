from abc import ABC, abstractmethod

from plants_classifier.core.dto import Plant


class IEnricher(ABC):
    @abstractmethod
    def enrich(self, name: str, data: Plant, allow_override_data: bool = False):
        """
        Gets name of plant and some additional data and tries to enrich this data

        :param name: name of plant
        :param data: some additional info known about plant before enrichment
        :param allow_override_data: is it allowed to override known fields
        """
        pass
