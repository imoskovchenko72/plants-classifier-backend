import traceback

from plants_classifier.common import AppLogger
from plants_classifier.core.dto import Plant
from plants_classifier.core.helpers import get_normalized_string

from .enrichers import PlantariumEnricher

logger = AppLogger(__name__)

ENRICHERS_CHAIN = (PlantariumEnricher(),)


def get_enriched_plant_model(plant: Plant, allow_override_data: bool = False) -> Plant:
    plant_copy = plant.copy(deep=True)
    _enrich_model(plant_copy, allow_override_data)
    return plant_copy


def create_enriched_plant_model(name: str) -> Plant:
    plant_model = Plant(name=get_normalized_string(name))
    _enrich_model(plant_model, False)
    return plant_model


def _enrich_model(plant_model: Plant, allow_override_data: bool = False):
    for enricher in ENRICHERS_CHAIN:
        try:
            enricher.enrich(
                name=get_normalized_string(plant_model.name),
                data=plant_model,
                allow_override_data=allow_override_data,
            )
        except Exception:
            logger.error(
                "Something went wrong during enrichment",
                enricher=str(type(enricher)),
                plant_name=plant_model.name,
                traceback=traceback.format_exc(),
            )

    check_consistency(plant_model)


def check_consistency(plant_model: Plant):
    if plant_model.division is None:
        logger.warn(f"Division not filled for plant {plant_model.name}")

    if plant_model.plant_class is None:
        logger.warn(f"Class not filled for plant {plant_model.name}")

    if plant_model.ordo is None:
        logger.warn(f"Ordo not filled for plant {plant_model.name}")

    if plant_model.familia is None:
        logger.warn(f"Familia not filled for plant {plant_model.name}")

    if plant_model.genus is None:
        logger.warn(f"Genus not filled for plant {plant_model.name}")

    if plant_model.species is None:
        logger.warn(f"Species not filled for plant {plant_model.name}")
