from sqlalchemy import ARRAY, BIGINT, TEXT, Column, UniqueConstraint

from plants_classifier.common import BaseDbModel


class TaxonModel(BaseDbModel):
    __tablename__ = "taxa"

    id = Column(BIGINT, primary_key=True, autoincrement=True, doc="Unique taxon id")
    name = Column(TEXT, nullable=False)
    type = Column(TEXT, nullable=False)
    description = Column(TEXT, nullable=True)
    russian_names = Column(ARRAY(TEXT), nullable=True, doc="Synonymous russian names")

    __table_args__ = (UniqueConstraint("name", "type", name="taxon_unique_key"),)
