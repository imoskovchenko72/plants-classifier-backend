from sqlalchemy import BIGINT, TEXT, TIMESTAMP, Column, ForeignKey
from sqlalchemy.sql import func

from plants_classifier.common import BaseDbModel

from .plant import PlantModel


class PhotoModel(BaseDbModel):
    __tablename__ = "photos"

    id = Column(TEXT, primary_key=True)

    created_at = Column(
        TIMESTAMP(timezone=True),
        server_default=func.current_timestamp(),
        nullable=False,
    )

    filename = Column(TEXT, nullable=True)
    description = Column(TEXT, nullable=True)
    plant_id = Column(
        BIGINT,
        ForeignKey(f"{PlantModel.__tablename__}.id", ondelete="CASCADE"),
        nullable=False,
    )
