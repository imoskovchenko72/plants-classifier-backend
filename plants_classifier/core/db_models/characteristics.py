from sqlalchemy import TEXT, Column

from plants_classifier.common import BaseDbModel


class CharacteristicsModel(BaseDbModel):
    __tablename__ = "characteristics"

    characteristic_type = Column(
        TEXT,
        primary_key=True,
        doc="type like 'life_form', 'leaf_type' or something else",
    )
    value = Column(TEXT, primary_key=True, doc="characteristic value")
