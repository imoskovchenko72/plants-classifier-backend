from sqlalchemy import ARRAY, BIGINT, BOOLEAN, TEXT, Column, ForeignKey

from plants_classifier.common import BaseDbModel

from .taxon import TaxonModel


class PlantModel(BaseDbModel):
    __tablename__ = "plants"

    id = Column(BIGINT, primary_key=True, autoincrement=True, doc="Unique plant id")
    name = Column(
        TEXT,
        nullable=False,
        doc="Scientific plant name usually looks like genus + species",
        unique=True,
    )
    description = Column(
        TEXT,
        nullable=True,
        doc="Some additional description which cannot be attributed to other characteristics",
    )
    russian_names = Column(ARRAY(TEXT), nullable=True, doc="Synonymous russian names")
    is_endangered = Column(
        BOOLEAN, nullable=False, default=False, doc="Is species endangered"
    )

    division_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Отдел",
    )
    plant_class_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Класс",
    )
    ordo_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Порядок",
    )
    familia_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Семейство",
    )
    genus_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Род",
    )
    species_id = Column(
        BIGINT,
        ForeignKey(f"{TaxonModel.__tablename__}.id", ondelete="RESTRICT"),
        nullable=True,
        doc="Вид",
    )

    life_form = Column(ARRAY(TEXT), nullable=True, doc="Жизненная форма")

    shoots_type = Column(ARRAY(TEXT), nullable=True, doc="Тип побегов")
    leaf_arrangement = Column(ARRAY(TEXT), nullable=True, doc="Листорасположение")
    leaf_placement = Column(ARRAY(TEXT), nullable=True, doc="Размещение листьев")
    shoots_surface = Column(ARRAY(TEXT), nullable=True, doc="Поверхность побегов")
    appendages = Column(ARRAY(TEXT), nullable=True, doc="Придатки")

    leaf_type = Column(ARRAY(TEXT), nullable=True, doc="Тип листьев")
    leaf_blade_shape = Column(
        ARRAY(TEXT), nullable=True, doc="Форма листовой пластинки"
    )
    leaf_blade_division = Column(
        ARRAY(TEXT), nullable=True, doc="Членение листовой пластинки"
    )
    leaf_blade_complexity_level = Column(
        ARRAY(TEXT), nullable=True, doc="Порядок сложности листовой пластинки"
    )
    leaf_blade_attachment = Column(
        ARRAY(TEXT), nullable=True, doc="Прикрепление листовой пластинки"
    )
    leaf_parts_shape = Column(ARRAY(TEXT), nullable=True, doc="Форма долей листьев")
    leaf_parts_division = Column(ARRAY(TEXT), nullable=True, doc="Листорасположение")
    leaf_apex = Column(ARRAY(TEXT), nullable=True, doc="Верхушка листа")
    leaf_margin = Column(ARRAY(TEXT), nullable=True, doc="Край листа")
    leaf_base = Column(ARRAY(TEXT), nullable=True, doc="Основание листа")
    leaf_surface = Column(ARRAY(TEXT), nullable=True, doc="Поверхность листа")
    leaf_appendages = Column(ARRAY(TEXT), nullable=True, doc="Придатки листа")

    inflorescense = Column(ARRAY(TEXT), nullable=True, doc="Соцветие")
    flower_main_colour = Column(ARRAY(TEXT), nullable=True, doc="Основной цвет цветка")
    flower_colour_tints = Column(ARRAY(TEXT), nullable=True, doc="Оттенки цветка")
    flower_spots_and_stripes = Column(
        ARRAY(TEXT), nullable=True, doc="Пятна и полоски цветка"
    )
    flower_size = Column(ARRAY(TEXT), nullable=True, doc="Размер цветка")
    perianth = Column(ARRAY(TEXT), nullable=True, doc="Околоцветник")
    petals_number = Column(ARRAY(TEXT), nullable=True, doc="Число лепестков")

    fruits_type = Column(ARRAY(TEXT), nullable=True, doc="Тип плодов")
    fruit_colour = Column(ARRAY(TEXT), nullable=True, doc="Окраска плодов")
    fruit_appendages = Column(ARRAY(TEXT), nullable=True, doc="Придатки плодов")

    cones_consistency = Column(ARRAY(TEXT), nullable=True, doc="Констистенция шишек")
    cones_shape = Column(ARRAY(TEXT), nullable=True, doc="Форма шишек")

    sporangia = Column(ARRAY(TEXT), nullable=True, doc="Спорангии")
