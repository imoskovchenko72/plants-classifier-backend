from sqlalchemy import BIGINT, TEXT, Column

from plants_classifier.common import BaseDbModel


class CharacteristicsSimilarityModel(BaseDbModel):
    """
    Note! It is not true similarity.
    It is table just to store info like
    'two characteristics values <first_value> and <second_value> were in the same plant together <count> times'
    """

    __tablename__ = "characteristics_similarity"

    characteristic_type = Column(
        TEXT,
        primary_key=True,
        doc="type like 'life_form', 'leaf_type' or something else",
    )
    first_value = Column(
        TEXT, primary_key=True, doc="first_value should be less than second_value"
    )
    second_value = Column(
        TEXT, primary_key=True, doc="first_value should be less than second_value"
    )

    count = Column(BIGINT, nullable=False, default=0)
