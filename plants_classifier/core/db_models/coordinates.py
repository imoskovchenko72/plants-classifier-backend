from sqlalchemy import BIGINT, DATE, DECIMAL, TEXT, TIMESTAMP, Column, ForeignKey
from sqlalchemy.sql import func

from plants_classifier.common import BaseDbModel

from .plant import PlantModel


class CoordinatesModel(BaseDbModel):
    __tablename__ = "coordinates"

    id = Column(BIGINT, primary_key=True, autoincrement=True)

    created_at = Column(
        TIMESTAMP(timezone=True),
        server_default=func.current_timestamp(),
        nullable=False,
        doc="Date and time of record creation",
    )

    date = Column(
        DATE, nullable=True, doc="Date when plant with such coordinates was found"
    )
    description = Column(TEXT, nullable=True)
    plant_id = Column(
        BIGINT,
        ForeignKey(f"{PlantModel.__tablename__}.id", ondelete="CASCADE"),
        nullable=False,
    )

    latitude = Column(DECIMAL, nullable=False)
    longitude = Column(DECIMAL, nullable=False)
