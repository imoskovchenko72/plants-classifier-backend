from plants_classifier.common import BaseBadRequestError, BaseNotFoundError


class TaxonAlreadyExistsError(BaseBadRequestError):
    message = "Taxon with such name and type already exists"


class TaxonNotFoundError(BaseNotFoundError):
    message = "Taxon with such id does not exist"


class TaxonUsedInPlantError(BaseBadRequestError):
    message = "Taxon used in some plant"


class TaxonTypeCannotBeChanged(BaseBadRequestError):
    message = "Taxon type can not be changed"
