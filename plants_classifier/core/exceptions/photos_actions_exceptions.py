from plants_classifier.common import BaseNotFoundError


class PhotoNotFoundError(BaseNotFoundError):
    message = "Photo with such id does not exist"
