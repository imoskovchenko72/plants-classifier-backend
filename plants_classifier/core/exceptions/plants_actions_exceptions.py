from plants_classifier.common import BaseBadRequestError, BaseNotFoundError


class PlantAlreadyExistsError(BaseBadRequestError):
    message = "Plant with such name already exists"


class PlantNotFoundError(BaseNotFoundError):
    message = "Plant with such id does not exist"
