from plants_classifier.common import BaseNotFoundError


class CoordinatesNotFoundError(BaseNotFoundError):
    message = "Coordinates with such id does not exist"
