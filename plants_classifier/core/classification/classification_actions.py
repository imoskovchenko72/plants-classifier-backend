import datetime
import math
from collections import defaultdict
from functools import lru_cache
from typing import Optional

import numpy
from numpy.typing import NDArray
from scipy.optimize import linear_sum_assignment
from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.constants import (
    COORDINATES,
    PlantCharacteristicType,
    TaxonType,
)
from plants_classifier.core.dto import (
    OutputTaxon,
    OutputTaxonWithRelevance,
    PlantCharacteristics,
    ShortOutputPlantInfo,
    ShortOutPutPlantInfoWithRelevance,
    SimpleCoordinates,
    TaxonomyFilter,
)
from plants_classifier.core.helpers import (
    convert_plant_model_to_short_info,
    get_normalized_string,
    get_plant_characteristics_from_db_model,
)
from plants_classifier.core.repositories import (
    CoordinatesRepository,
    PlantRepository,
    TaxonRepository,
)
from plants_classifier.core.settings import get_config

from .estimators import FrequencySimilarityEstimator, ICharacteristicEstimator


def build_weights_matrix(
    characteristic: PlantCharacteristicType,
    a: list[str],
    b: list[str],
    estimator: ICharacteristicEstimator,
) -> NDArray:
    python_array = [
        [
            estimator.estimate_similarity(
                characteristic, get_normalized_string(a[i]), get_normalized_string(b[j])
            )
            for j in range(len(b))
        ]
        for i in range(len(a))
    ]
    return numpy.array(python_array)


def calculate_characteristics_values_distance(
    characteristic: PlantCharacteristicType,
    desired_values: list[str],
    real_values: list[str],
    estimator: ICharacteristicEstimator,
) -> float:
    if not desired_values:
        return 0
    if not real_values:
        return 1
    weights_matrix = build_weights_matrix(
        characteristic,
        desired_values,
        real_values,
        estimator,
    )
    row_ind, col_ind = linear_sum_assignment(weights_matrix)
    best_sum = weights_matrix[row_ind, col_ind].sum()
    if len(desired_values) > len(real_values):
        best_sum += len(desired_values) - len(real_values)
    return best_sum / len(desired_values)


def calculate_distance_between_coordinates(
    coordinate_a: SimpleCoordinates, coordinate_b: SimpleCoordinates
) -> float:
    R = 6371

    d_lat = math.radians(coordinate_b.latitude - coordinate_a.latitude)
    d_lon = math.radians(coordinate_b.longitude - coordinate_a.longitude)

    a = math.sin(d_lat / 2) * math.sin(d_lat / 2) + math.cos(
        math.radians(coordinate_a.latitude)
    ) * math.cos(math.radians(coordinate_b.latitude)) * math.sin(d_lon / 2) * math.sin(
        d_lon / 2
    )
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c
    return distance


def calculate_coordinates_distance(
    desired_coordinates: Optional[SimpleCoordinates],
    real_coordinates_list: list[SimpleCoordinates],
) -> float:
    if desired_coordinates is None:
        return 0
    if len(real_coordinates_list) == 0:
        return 0

    total_distance = 0
    for real_coordinate in real_coordinates_list:
        total_distance += calculate_distance_between_coordinates(
            real_coordinate, desired_coordinates
        )
    mean_distance = total_distance / len(real_coordinates_list)

    if mean_distance >= 2000:
        return 1
    return 1 - (2000 - mean_distance) / 2000


def calculate_plant_distance(
    desired_characteristics: PlantCharacteristics,
    desired_coordinates: Optional[SimpleCoordinates],
    real_plant_characteristics: PlantCharacteristics,
    real_coordinates_list: list[SimpleCoordinates],
    estimator: ICharacteristicEstimator,
) -> float:
    total_distance = 0.0

    desired_characteristics_dict = desired_characteristics.model_dump()
    real_plant_characteristics_dict = real_plant_characteristics.model_dump()

    for characteristic in PlantCharacteristicType:
        desired_characteristic_value = (
            desired_characteristics_dict.get(characteristic.value) or []
        )
        real_plant_characteristic_value = (
            real_plant_characteristics_dict.get(characteristic.value) or []
        )

        importance_coefficient = (
            get_config().classification.characteristics_importance_coefficients.get(
                characteristic.value, 1
            )
        )
        total_distance += importance_coefficient * (
            calculate_characteristics_values_distance(
                characteristic,
                desired_characteristic_value,
                real_plant_characteristic_value,
                estimator,
            )
            ** 2
        )
    total_distance += (
        get_config().classification.characteristics_importance_coefficients.get(
            COORDINATES, 1
        )
        * (
            calculate_coordinates_distance(desired_coordinates, real_coordinates_list)
            ** 2
        )
    )

    return total_distance


@lru_cache(maxsize=1)
def get_max_distance() -> float:
    total_distance = 0
    for characteristic in PlantCharacteristicType:
        importance_coefficient = (
            get_config().classification.characteristics_importance_coefficients.get(
                characteristic.value, 1
            )
        )
        total_distance += importance_coefficient
    total_distance += (
        get_config().classification.characteristics_importance_coefficients.get(
            COORDINATES, 1
        )
    )
    return total_distance


@with_session()
def get_relevant_for_classification_coordinates(
    db_session: Session,
) -> dict[int, list[SimpleCoordinates]]:
    all_coordinates: dict[int, list[SimpleCoordinates]] = defaultdict(list)
    for coordinate in CoordinatesRepository().all(db_session=db_session):
        if datetime.datetime.now() - coordinate.created_at.replace(
            tzinfo=None
        ) > datetime.timedelta(days=7):
            all_coordinates[coordinate.plant_id].append(
                SimpleCoordinates(
                    latitude=coordinate.latitude, longitude=coordinate.longitude
                )
            )
    return all_coordinates


@with_session()
def get_most_relevant_plants(
    characteristics: PlantCharacteristics,
    db_session: Session,
    coordinates: Optional[SimpleCoordinates] = None,
    taxonomy_filter: Optional[TaxonomyFilter] = None,
) -> list[ShortOutPutPlantInfoWithRelevance]:
    estimator = FrequencySimilarityEstimator(db_session=db_session)

    all_coordinates = get_relevant_for_classification_coordinates(db_session=db_session)

    plants_with_distance: list[tuple[float, ShortOutputPlantInfo]] = []

    for plant in PlantRepository().all_with_taxonomy(
        taxonomy=taxonomy_filter, db_session=db_session
    ):
        plants_with_distance.append(
            (
                calculate_plant_distance(
                    characteristics,
                    coordinates,
                    get_plant_characteristics_from_db_model(plant),
                    all_coordinates[plant.id],
                    estimator,
                ),
                convert_plant_model_to_short_info(plant, db_session=db_session),
            )
        )

    plants_with_distance.sort(key=lambda t: t[0])

    max_distance = get_max_distance()
    return [
        ShortOutPutPlantInfoWithRelevance(
            **t[1].model_dump(mode="json"),
            relevance_ratio=(max_distance - t[0]) / max_distance,
        )
        for t in plants_with_distance
    ]


@with_session()
def get_most_relevant_taxa(
    characteristics: PlantCharacteristics,
    db_session: Session,
    coordinates: Optional[SimpleCoordinates] = None,
) -> list[OutputTaxonWithRelevance]:
    estimator = FrequencySimilarityEstimator(db_session=db_session)

    taxon_plants_count: dict[int, int] = defaultdict(int)
    taxon_plants_distance: dict[int, float] = defaultdict(float)

    all_coordinates = get_relevant_for_classification_coordinates(db_session=db_session)

    for plant in PlantRepository().all(db_session=db_session):
        distance = calculate_plant_distance(
            characteristics,
            coordinates,
            get_plant_characteristics_from_db_model(plant),
            all_coordinates[plant.id],
            estimator,
        )

        plant_dict = plant.to_dict()
        for taxon_type in TaxonType:
            taxon_id = plant_dict.get(f"{taxon_type.value}_id", None)
            if taxon_id is not None:
                taxon_plants_count[taxon_id] += 1
                taxon_plants_distance[taxon_id] += distance

    taxa_with_relevance: list[tuple[float, OutputTaxon]] = []

    max_distance_of_plant = get_max_distance()
    for taxon in TaxonRepository().all(db_session=db_session):
        if taxon.type == TaxonType.SPECIES.value:
            continue
        if taxon_plants_count[taxon.id] == 0:
            continue
        max_distance = taxon_plants_count[taxon.id] * max_distance_of_plant
        actual_distance = taxon_plants_distance[taxon.id]

        taxa_with_relevance.append(
            (
                (max_distance - actual_distance) / max_distance,
                OutputTaxon(**taxon.to_dict()),
            )
        )

    taxa_with_relevance.sort(key=lambda t: -t[0])
    return [
        OutputTaxonWithRelevance(**t[1].model_dump(mode="json"), relevance_ratio=t[0])
        for t in taxa_with_relevance
    ]
