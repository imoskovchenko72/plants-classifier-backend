from typing import Optional

from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.constants import TaxonType
from plants_classifier.core.dto import OutputTaxon, ShortOutputPlantInfo, TaxonomyFilter
from plants_classifier.core.helpers import convert_plant_model_to_short_info, split_text
from plants_classifier.core.repositories import PlantRepository, TaxonRepository


def calculate_words_sequences_similarity(
    query_words: list[str], real_words: list[str]
) -> float:
    score = 0
    for query_word in query_words:
        max_score_for_word = 0
        for real_word in real_words:
            if real_word.startswith(query_word):
                max_score_for_word = max(
                    max_score_for_word, len(query_word) / len(real_word)
                )

        score += max_score_for_word

    return score


@with_session()
def get_most_relevant_plants_by_query(
    query: str,
    taxonomy_filter: Optional[TaxonomyFilter],
    db_session: Session,
) -> list[ShortOutputPlantInfo]:
    query_words = split_text(query)

    plants_with_similarity_score: list[tuple[float, ShortOutputPlantInfo]] = []

    for plant in PlantRepository().all_with_taxonomy(
        taxonomy=taxonomy_filter, db_session=db_session
    ):
        score = calculate_words_sequences_similarity(
            query_words, split_text(plant.name)
        )
        if plant.russian_names:
            for russian_name in plant.russian_names:
                score = max(
                    score,
                    calculate_words_sequences_similarity(
                        query_words, split_text(russian_name)
                    ),
                )

        plants_with_similarity_score.append(
            (
                score,
                convert_plant_model_to_short_info(plant, db_session=db_session),
            )
        )

    plants_with_similarity_score.sort(key=lambda t: -t[0])
    return [t[1] for t in plants_with_similarity_score if t[0] > 0]


@with_session()
def get_most_relevant_taxa_by_query(
    query: str, taxon_type: TaxonType, db_session: Session
) -> list[OutputTaxon]:
    query_words = split_text(query)

    taxa_with_similarity_score: list[tuple[float, OutputTaxon]] = []

    for taxon in TaxonRepository().all(db_session=db_session):
        if taxon_type is not None and taxon_type.value != taxon.type:
            continue
        score = calculate_words_sequences_similarity(
            query_words, split_text(taxon.name)
        )
        if taxon.russian_names:
            for russian_name in taxon.russian_names:
                score = max(
                    score,
                    calculate_words_sequences_similarity(
                        query_words, split_text(russian_name)
                    ),
                )

        taxa_with_similarity_score.append(
            (
                score,
                OutputTaxon(**taxon.to_dict()),
            )
        )

    taxa_with_similarity_score.sort(key=lambda t: -t[0])
    return [t[1] for t in taxa_with_similarity_score if t[0] > 0]
