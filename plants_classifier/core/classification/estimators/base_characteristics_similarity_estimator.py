from abc import ABC, abstractmethod

from plants_classifier.core.constants import PlantCharacteristicType


class ICharacteristicEstimator(ABC):
    @abstractmethod
    def estimate_similarity(
        self,
        characteristic: PlantCharacteristicType,
        first_value: str,
        second_value: str,
    ) -> float:
        """

        :param characteristic: some plant characteristic which values belong to
        :param first_value: some word or phrase which is the value of characteristic
        :param second_value: another some word or phrase which is the value of characteristic

        :return: real number from range [0; 1]
        """
        pass
