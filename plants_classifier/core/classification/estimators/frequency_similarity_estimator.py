from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.constants import PlantCharacteristicType

from .base_characteristics_similarity_estimator import ICharacteristicEstimator
from .characteristics_similarity_cache import CharacteristicsSimilarityCache


class FrequencySimilarityEstimator(ICharacteristicEstimator):
    @with_session()
    def __init__(self, db_session: Session):
        self._cache = CharacteristicsSimilarityCache(db_session=db_session)

    def estimate_similarity(
        self,
        characteristic: PlantCharacteristicType,
        first_value: str,
        second_value: str,
    ) -> float:
        if first_value == second_value:
            return 0

        values_frequency = self._cache.get_count_by_values(
            characteristic, first_value, second_value
        )
        max_characteristic_frequency = self._cache.get_count_by_values(
            characteristic, first_value, first_value
        )

        if values_frequency == 0:
            return 1

        return (
            max_characteristic_frequency - values_frequency
        ) / max_characteristic_frequency
