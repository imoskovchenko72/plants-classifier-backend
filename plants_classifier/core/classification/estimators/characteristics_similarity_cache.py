from collections import defaultdict

from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.constants import PlantCharacteristicType
from plants_classifier.core.repositories import CharacteristicsSimilarityRepository


class CharacteristicsSimilarityCache:
    @with_session()
    def __init__(self, db_session: Session):
        self._max_by_characteristic_type = defaultdict(int)
        self._similarity = defaultdict(dict)

        for characteristics_pair in CharacteristicsSimilarityRepository().all(
            db_session=db_session
        ):
            self._max_by_characteristic_type[
                characteristics_pair.characteristic_type
            ] = max(
                self._max_by_characteristic_type[
                    characteristics_pair.characteristic_type
                ],
                characteristics_pair.count,
            )
            self._similarity[characteristics_pair.characteristic_type][
                self._get_sorted_tuple(
                    characteristics_pair.first_value, characteristics_pair.second_value
                )
            ] = characteristics_pair.count

    def get_count_by_values(
        self,
        characteristic: PlantCharacteristicType,
        first_value: str,
        second_value: str,
    ) -> int:
        return self._similarity[characteristic.value].get(
            self._get_sorted_tuple(first_value, second_value), 0
        )

    def get_max_by_characteristic_type(
        self, characteristic: PlantCharacteristicType
    ) -> int:
        return self._max_by_characteristic_type[characteristic.value]

    @staticmethod
    def _get_sorted_tuple(a: str, b: str) -> tuple[str, str]:
        return min(a, b), max(a, b)
