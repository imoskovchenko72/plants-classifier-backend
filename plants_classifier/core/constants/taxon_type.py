from enum import Enum


class TaxonType(Enum):
    DIVISION = "division"  # отдел
    CLASS = "plant_class"  # класс
    ORDO = "ordo"  # порядок
    FAMILIA = "familia"  # семейство
    GENUS = "genus"  # род
    SPECIES = "species"  # вид
