from enum import Enum


class PlantCharacteristicType(Enum):
    LIFE_FROM = "life_form"  # жизненная форма

    SHOOTS_TYPE = "shoots_type"  # тип побегов
    LEAF_ARRANGEMENT = "leaf_arrangement"  # листорасположение
    LEAF_PLACEMENT = "leaf_placement"  # размещение листьев
    SHOOTS_SURFACE = "shoots_surface"  # поверхность побегов
    APPENDAGES = "appendages"  # придатки

    LEAF_TYPE = "leaf_type"  # тип листьев
    LEAF_BLADE_SHAPE = "leaf_blade_shape"  # форма листовой пластинки
    LEAF_BLADE_DIVISION = "leaf_blade_division"  # членение листовой пластинки
    LEAF_BLADE_COMPLEXITY_LEVEL = (
        "leaf_blade_complexity_level"  # порядок сложности листовой пластинки
    )
    LEAF_BLADE_ATTACHMENT = "leaf_blade_attachment"  # прикрепление листовой пластинки
    LEAF_PARTS_SHAPE = "leaf_parts_shape"  # форма долей листьев
    LEAF_PARTS_DIVISION = "leaf_parts_division"  # членение долей листьев
    LEAF_APEX = "leaf_apex"  # верхушка листа
    LEAF_MARGIN = "leaf_margin"  # край листа
    LEAF_BASE = "leaf_base"  # основание листа
    LEAF_SURFACE = "leaf_surface"  # поверхность листа
    LEAF_APPENDAGES = "leaf_appendages"  # придатки листа

    INFLORESCENSE = "inflorescense"  # соцветие
    FLOWER_MAIN_COLOUR = "flower_main_colour"  # основной цвет цветка
    FLOWER_COLOUR_TINTS = "flower_colour_tints"  # оттенки цветка
    FLOWER_SPOTS_AND_STRIPES = "flower_spots_and_stripes"  # пятна и полоски цветка
    FLOWER_SIZE = "flower_size"  # размер цветка
    PERIANTH = "perianth"  # околоцветник
    PETALS_NUMBER = "petals_number"  # число лепестков

    FRUITS_TYPE = "fruits_type"  # тип плодов
    FRUIT_COLOUR = "fruit_colour"  # окраска плодов
    FRUIT_APPENDAGES = "fruit_appendages"  # придатки плодов

    CONES_CONSISTENCY = "cones_consistency"  # консистенция шишек
    CONES_SHAPE = "cones_shape"  # форма шишек

    SPORANGIA = "sporangia"  # спорангии


COORDINATES = "coordinates"
