from typing import Optional

from pydantic import BaseModel

from plants_classifier.core.constants import TaxonType

from .relevance import WithRelevanceMixin


class Taxon(BaseModel):
    name: str
    type: TaxonType
    description: Optional[str]
    russian_names: Optional[list[str]]


class OutputTaxon(Taxon):
    id: int


class OutputTaxonWithRelevance(OutputTaxon, WithRelevanceMixin):
    pass


class OutputShortTaxonInfo(BaseModel):
    id: int
    name: str


class TaxonomyFilter(BaseModel):
    taxon_id: Optional[int] = None
