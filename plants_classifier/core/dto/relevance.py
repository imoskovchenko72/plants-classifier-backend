from pydantic import BaseModel


class WithRelevanceMixin(BaseModel):
    relevance_ratio: float
