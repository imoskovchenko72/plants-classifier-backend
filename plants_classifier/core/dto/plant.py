from typing import Optional

from pydantic import BaseModel

from .pagination import BasePagination
from .relevance import WithRelevanceMixin
from .taxon import OutputShortTaxonInfo, TaxonomyFilter


class PlantCharacteristics(BaseModel):
    life_form: Optional[list[str]] = None  # жизненная форма

    shoots_type: Optional[list[str]] = None  # тип побегов
    leaf_arrangement: Optional[list[str]] = None  # листорасположение
    leaf_placement: Optional[list[str]] = None  # размещение листьев
    shoots_surface: Optional[list[str]] = None  # поверхность побегов
    appendages: Optional[list[str]] = None  # придатки

    leaf_type: Optional[list[str]] = None  # тип листьев
    leaf_blade_shape: Optional[list[str]] = None  # форма листовой пластинки
    leaf_blade_division: Optional[list[str]] = None  # членение листовой пластинки
    leaf_blade_complexity_level: Optional[
        list[str]
    ] = None  # порядок сложности листовой пластинки
    leaf_blade_attachment: Optional[list[str]] = None  # прикрепление листовой пластинки
    leaf_parts_shape: Optional[list[str]] = None  # форма долей листьев
    leaf_parts_division: Optional[list[str]] = None  # членение долей листьев
    leaf_apex: Optional[list[str]] = None  # верхушка листа
    leaf_margin: Optional[list[str]] = None  # край листа
    leaf_base: Optional[list[str]] = None  # основание листа
    leaf_surface: Optional[list[str]] = None  # поверхность листа
    leaf_appendages: Optional[list[str]] = None  # придатки листа

    inflorescense: Optional[list[str]] = None  # соцветие
    flower_main_colour: Optional[list[str]] = None  # основной цвет цветка
    flower_colour_tints: Optional[list[str]] = None  # оттенки цветка
    flower_spots_and_stripes: Optional[list[str]] = None  # пятна и полоски цветка
    flower_size: Optional[list[str]] = None  # размер цветка
    perianth: Optional[list[str]] = None  # околоцветник
    petals_number: Optional[list[str]] = None  # число лепестков

    fruits_type: Optional[list[str]] = None  # тип плодов
    fruit_colour: Optional[list[str]] = None  # окраска плодов
    fruit_appendages: Optional[list[str]] = None  # придатки плодов

    cones_consistency: Optional[list[str]] = None  # консистенция шишек
    cones_shape: Optional[list[str]] = None  # форма шишек

    sporangia: Optional[list[str]] = None  # спорангии


class NamedPlant(BaseModel):
    name: str  # латинское название
    is_endangered: bool  # является ли вымирающим

    russian_names: Optional[list[str]] = None  # русские названия
    description: Optional[str] = None  # некоторое дополнительное описание


class Plant(NamedPlant, PlantCharacteristics):
    division: Optional[str] = None  # отдел
    plant_class: Optional[str] = None  # класс
    ordo: Optional[str] = None  # порядок
    familia: Optional[str] = None  # семейство
    genus: Optional[str] = None  # род
    species: Optional[str] = None  # вид


class PlantWithId(Plant):
    id: int


class OutputPlant(NamedPlant, PlantCharacteristics):
    id: int

    division: Optional[OutputShortTaxonInfo] = None  # отдел
    plant_class: Optional[OutputShortTaxonInfo] = None  # класс
    ordo: Optional[OutputShortTaxonInfo] = None  # порядок
    familia: Optional[OutputShortTaxonInfo] = None  # семейство
    genus: Optional[OutputShortTaxonInfo] = None  # род
    species: Optional[OutputShortTaxonInfo] = None  # вид


class ShortOutputPlantInfo(BaseModel):
    id: int
    name: str
    is_endangered: bool
    russian_name: Optional[str] = None
    photo_id: Optional[str] = None


class ShortOutPutPlantInfoWithRelevance(ShortOutputPlantInfo, WithRelevanceMixin):
    pass


class PlantByTaxonomyRequest(BasePagination):
    taxonomy: Optional[TaxonomyFilter] = None
