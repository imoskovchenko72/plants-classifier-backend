from typing import Optional

from pydantic import BaseModel


class FileContent(BaseModel):
    data: bytes


class PhotoInfo(BaseModel):
    description: Optional[str] = None
    filename: str
    plant_id: int


class OutputPhotoInfo(PhotoInfo):
    id: str


class PhotoWithData(PhotoInfo, FileContent):
    pass
