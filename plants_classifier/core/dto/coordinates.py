import datetime
from decimal import Decimal
from typing import Optional

from pydantic import BaseModel


class SimpleCoordinates(BaseModel):
    latitude: Decimal
    longitude: Decimal


class CoordinatesInfo(SimpleCoordinates):
    description: Optional[str] = None
    plant_id: int
    date: Optional[datetime.date] = None


class OutputCoordinatesInfo(CoordinatesInfo):
    id: int


class ShortCoordinatesInfo(BaseModel):
    id: int
    latitude: Decimal
    longitude: Decimal
