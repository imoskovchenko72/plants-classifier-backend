from typing import Optional

from .coordinates import SimpleCoordinates
from .pagination import BasePagination
from .plant import PlantCharacteristics
from .taxon import TaxonomyFilter


class ClassifyPlantRequest(BasePagination):
    taxonomy: Optional[TaxonomyFilter] = None
    characteristics: PlantCharacteristics
    coordinates: Optional[SimpleCoordinates] = None


class ClassifyTaxonRequest(BasePagination):
    characteristics: PlantCharacteristics
    coordinates: Optional[SimpleCoordinates] = None
