from typing import Optional

from plants_classifier.core.constants import TaxonType

from .pagination import BasePagination
from .taxon import TaxonomyFilter


class SearchPlantRequest(BasePagination):
    taxonomy: Optional[TaxonomyFilter] = None
    query: str


class SearchTaxonRequest(BasePagination):
    query: str
    taxon_type: Optional[TaxonType] = None
