from pydantic import BaseModel

from plants_classifier.core.constants import UNLIMITED


class BasePagination(BaseModel):
    limit: int = UNLIMITED
    offset: int = 0
