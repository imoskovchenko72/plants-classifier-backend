from pydantic import BaseModel


class CharacteristicsGroups(BaseModel):
    shoots: list[str]
    leaves: list[str]
    flowers: list[str]
    fruits: list[str]
    cones: list[str]
    sporangia: list[str]
