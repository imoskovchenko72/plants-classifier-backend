from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.db_models import PlantModel
from plants_classifier.core.dto import (
    OutputPlant,
    Plant,
    PlantByTaxonomyRequest,
    PlantWithId,
    ShortOutputPlantInfo,
)
from plants_classifier.core.exceptions import (
    PlantAlreadyExistsError,
    PlantNotFoundError,
)
from plants_classifier.core.helpers import (
    convert_plant_db_model_to_dto,
    convert_plant_model_to_short_info,
    get_normalized_string,
    get_page_part,
    remove_characteristics,
    remove_photos,
    update_characteristics_similarity,
    update_taxa,
)
from plants_classifier.core.repositories import PlantRepository


@with_session()
def get_plant(request: int, db_session: Session) -> OutputPlant:
    plant = PlantRepository().get_by_id(id=request, db_session=db_session)
    if plant is None:
        raise PlantNotFoundError()

    return convert_plant_db_model_to_dto(plant_model=plant, db_session=db_session)


@with_session()
def save_plant(request: Plant, db_session: Session) -> OutputPlant:
    plant_repository = PlantRepository()
    if plant_repository.get_by_name(
        name=get_normalized_string(request.name), db_session=db_session
    ):
        raise PlantAlreadyExistsError()

    plant_model = PlantModel(
        name=get_normalized_string(request.name),
        russian_names=request.russian_names,
        is_endangered=request.is_endangered,
        description=request.description,
    )
    update_taxa(plant_dto=request, plant_db_model=plant_model, db_session=db_session)
    update_characteristics_similarity(
        plant_dto=request, plant_db_model=plant_model, db_session=db_session
    )

    plant_repository.save(model=plant_model, db_session=db_session)
    return convert_plant_db_model_to_dto(plant_model=plant_model, db_session=db_session)


@with_session()
def delete_plant(request: int, db_session: Session):
    plant_repository = PlantRepository()

    plant_model = plant_repository.get_by_id(id=request, db_session=db_session)
    if plant_model is None:
        return

    remove_characteristics(plant_db_model=plant_model, db_session=db_session)
    remove_photos(plant_id=plant_model.id, db_session=db_session)
    plant_repository.delete(model=plant_model, db_session=db_session)


@with_session()
def update_plant(request: PlantWithId, db_session: Session) -> OutputPlant:
    plant_repository = PlantRepository()

    plant_model = plant_repository.get_by_id(id=request.id, db_session=db_session)
    if plant_model is None:
        raise PlantNotFoundError()
    maybe_existing_plant = plant_repository.get_by_name(
        get_normalized_string(request.name), db_session=db_session
    )
    if maybe_existing_plant is not None and maybe_existing_plant.id != request.id:
        raise PlantAlreadyExistsError()

    remove_characteristics(plant_db_model=plant_model, db_session=db_session)

    plant_model.name = get_normalized_string(request.name)
    plant_model.russian_names = request.russian_names
    plant_model.is_endangered = request.is_endangered
    plant_model.description = request.description

    update_taxa(plant_dto=request, plant_db_model=plant_model, db_session=db_session)
    update_characteristics_similarity(
        plant_dto=request, plant_db_model=plant_model, db_session=db_session
    )

    plant_repository.save(model=plant_model, db_session=db_session)
    return convert_plant_db_model_to_dto(plant_model=plant_model, db_session=db_session)


@with_session()
def get_plants_by_taxonomy(
    request: PlantByTaxonomyRequest, db_session: Session
) -> list[ShortOutputPlantInfo]:
    plants = [
        convert_plant_model_to_short_info(plant, db_session=db_session)
        for plant in PlantRepository().all_with_taxonomy(
            taxonomy=request.taxonomy, db_session=db_session
        )
    ]

    return get_page_part(plants, request.limit, request.offset)
