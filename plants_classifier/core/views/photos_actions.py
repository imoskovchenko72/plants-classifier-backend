from sqlalchemy.orm import Session

from plants_classifier.common import get_file_storage, with_session
from plants_classifier.core.dto import OutputPhotoInfo, PhotoWithData
from plants_classifier.core.exceptions import PhotoNotFoundError
from plants_classifier.core.helpers import get_reduced_image
from plants_classifier.core.repositories import PhotosRepository


@with_session()
def save_photo(request: PhotoWithData, db_session: Session) -> OutputPhotoInfo:
    file_storage = get_file_storage()
    repository = PhotosRepository()

    photo_id = file_storage.save(request.data)
    photo_model = repository.create(
        id=photo_id,
        filename=request.filename,
        description=request.description,
        plant_id=request.plant_id,
        db_session=db_session,
    )
    return OutputPhotoInfo(**photo_model.to_dict())


@with_session()
def delete_photo(request: str, db_session: Session):
    file_storage = get_file_storage()
    repository = PhotosRepository()

    photo = repository.get_by_id(request, db_session=db_session)
    if photo is None:
        return

    file_storage.delete(request)
    repository.delete(model=photo, db_session=db_session)


@with_session()
def get_photo(request: str, db_session: Session) -> bytes:
    file_storage = get_file_storage()
    repository = PhotosRepository()

    if repository.get_by_id(request, db_session=db_session) is None:
        raise PhotoNotFoundError()

    return file_storage.get(request)


@with_session()
def get_photo_miniature(request: str, db_session: Session) -> bytes:
    file_storage = get_file_storage()
    repository = PhotosRepository()

    if repository.get_by_id(request, db_session=db_session) is None:
        raise PhotoNotFoundError()

    photo_bytes = file_storage.get(request)

    return get_reduced_image(photo_bytes)


@with_session()
def get_all_plant_photos(request: int, db_session: Session) -> list[OutputPhotoInfo]:
    photos_list = PhotosRepository().all_by_plant_id(
        plant_id=request, db_session=db_session
    )
    return [OutputPhotoInfo(**photo_model.to_dict()) for photo_model in photos_list]
