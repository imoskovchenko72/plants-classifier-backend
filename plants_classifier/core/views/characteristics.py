from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.constants import PlantCharacteristicType
from plants_classifier.core.repositories import CharacteristicsRepository


@with_session()
def get_all_characteristics_by_type(
    request: PlantCharacteristicType, db_session: Session
) -> list[str]:
    return CharacteristicsRepository().get_all_by_characteristic_type(
        characteristic_type=request, db_session=db_session
    )
