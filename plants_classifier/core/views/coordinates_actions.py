from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.dto import CoordinatesInfo, OutputCoordinatesInfo
from plants_classifier.core.repositories import CoordinatesRepository


@with_session()
def save_coordinates(
    request: CoordinatesInfo, db_session: Session
) -> OutputCoordinatesInfo:
    coordinates_model = CoordinatesRepository().create(
        **request.model_dump(), db_session=db_session
    )
    return OutputCoordinatesInfo(**coordinates_model.to_dict())


@with_session()
def delete_coordinates(request: int, db_session: Session):
    repository = CoordinatesRepository()

    coordinates_model = repository.get_by_id(id=request, db_session=db_session)
    if coordinates_model is None:
        return

    repository.delete(model=coordinates_model, db_session=db_session)


@with_session()
def get_all_plant_coordinates(
    request: int, db_session: Session
) -> list[OutputCoordinatesInfo]:
    coordinates_list = CoordinatesRepository().all_by_plant_id(
        plant_id=request, db_session=db_session
    )
    return [
        OutputCoordinatesInfo(**coordinates_model.to_dict())
        for coordinates_model in coordinates_list
    ]
