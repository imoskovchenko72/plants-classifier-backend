from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.classification import (
    get_most_relevant_plants_by_query,
    get_most_relevant_taxa_by_query,
)
from plants_classifier.core.dto import (
    OutputTaxon,
    SearchPlantRequest,
    SearchTaxonRequest,
    ShortOutputPlantInfo,
)
from plants_classifier.core.helpers import get_page_part


@with_session()
def get_plants_by_search_query(
    request: SearchPlantRequest, db_session: Session
) -> list[ShortOutputPlantInfo]:
    plants = get_most_relevant_plants_by_query(
        query=request.query, taxonomy_filter=request.taxonomy, db_session=db_session
    )
    return get_page_part(plants, request.limit, request.offset)


@with_session()
def get_taxa_by_search_query(
    request: SearchTaxonRequest, db_session: Session
) -> list[OutputTaxon]:
    taxa = get_most_relevant_taxa_by_query(
        query=request.query, taxon_type=request.taxon_type, db_session=db_session
    )
    return get_page_part(taxa, request.limit, request.offset)
