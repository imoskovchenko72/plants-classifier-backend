from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.dto import OutputTaxon, Taxon
from plants_classifier.core.exceptions import (
    TaxonAlreadyExistsError,
    TaxonNotFoundError,
    TaxonTypeCannotBeChanged,
    TaxonUsedInPlantError,
)
from plants_classifier.core.helpers import get_normalized_string
from plants_classifier.core.repositories import TaxonRepository


@with_session()
def get_taxon(request: int, db_session: Session) -> OutputTaxon:
    taxon_repository = TaxonRepository()
    taxon = taxon_repository.get_by_id(id=request, db_session=db_session)
    if taxon is None:
        raise TaxonNotFoundError()

    return OutputTaxon(**taxon.to_dict())


@with_session()
def create_taxon(request: Taxon, db_session: Session) -> OutputTaxon:
    request.name = get_normalized_string(request.name)

    taxon_repository = TaxonRepository()
    taxon = taxon_repository.get_by_name_and_type(
        name=request.name, taxon_type=request.type, db_session=db_session
    )
    if taxon is not None:
        raise TaxonAlreadyExistsError()

    taxon_model = taxon_repository.create(
        **request.model_dump(mode="json"), db_session=db_session
    )
    return OutputTaxon(**taxon_model.to_dict())


@with_session()
def delete_taxon(request: int, db_session: Session):
    taxon_repository = TaxonRepository()
    taxon = taxon_repository.get_by_id(id=request, db_session=db_session)
    if taxon is None:
        return

    try:
        taxon_repository.delete(model=taxon, db_session=db_session)
    except IntegrityError:
        raise TaxonUsedInPlantError()


@with_session()
def update_taxon(request: OutputTaxon, db_session: Session) -> OutputTaxon:
    request.name = get_normalized_string(request.name)

    taxon_repository = TaxonRepository()
    taxon = taxon_repository.get_by_id(id=request.id, db_session=db_session)
    if taxon is None:
        raise TaxonNotFoundError()
    maybe_existing_taxon = taxon_repository.get_by_name_and_type(
        name=request.name, taxon_type=request.type, db_session=db_session
    )
    if maybe_existing_taxon is not None and maybe_existing_taxon.id != request.id:
        raise TaxonAlreadyExistsError()

    if taxon.type != request.type.value:
        raise TaxonTypeCannotBeChanged()

    taxon.name = request.name
    taxon.type = request.type.value
    taxon.description = request.description
    taxon.russian_names = request.russian_names

    taxon_repository.save(taxon, db_session=db_session)
    return OutputTaxon(**taxon.to_dict())
