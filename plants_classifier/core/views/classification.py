from sqlalchemy.orm import Session

from plants_classifier.common import with_session
from plants_classifier.core.classification import (
    get_most_relevant_plants,
    get_most_relevant_taxa,
)
from plants_classifier.core.dto import (
    ClassifyPlantRequest,
    ClassifyTaxonRequest,
    OutputTaxonWithRelevance,
    ShortOutPutPlantInfoWithRelevance,
)
from plants_classifier.core.helpers import get_page_part


@with_session()
def get_plants_by_characteristics(
    request: ClassifyPlantRequest, db_session: Session
) -> list[ShortOutPutPlantInfoWithRelevance]:
    plants = get_most_relevant_plants(
        characteristics=request.characteristics,
        coordinates=request.coordinates,
        taxonomy_filter=request.taxonomy,
        db_session=db_session,
    )
    return get_page_part(plants, request.limit, request.offset)


@with_session()
def get_taxa_by_characteristics(
    request: ClassifyTaxonRequest, db_session: Session
) -> list[OutputTaxonWithRelevance]:
    taxa = get_most_relevant_taxa(
        characteristics=request.characteristics,
        coordinates=request.coordinates,
        db_session=db_session,
    )

    return get_page_part(taxa, request.limit, request.offset)
