from plants_classifier.core.dto import Plant
from plants_classifier.core.enrichment import get_enriched_plant_model


def enrich_plant(request: Plant) -> Plant:
    return get_enriched_plant_model(request, False)
