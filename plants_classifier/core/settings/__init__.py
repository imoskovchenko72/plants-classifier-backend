import os
import pathlib
from typing import Optional

from plants_classifier.common import ConfigFactory, init_common_config_models

from .config_models import ApplicationConfig

_config: Optional[ApplicationConfig] = None

DEFAULT_CONFIG_PATH = os.path.join(
    pathlib.Path(__file__).parent.resolve(), "..", "..", "config", "config.yaml"
)
DEFAULT_ENVIRONMENT_PATH = os.path.join(
    pathlib.Path(__file__).parent.resolve(),
    "..",
    "..",
    "config",
    "dev_environment.yaml",
)


def init_config(
    config_paths: tuple[str] = (
        os.getenv("APP_CONFIG_PATH", DEFAULT_CONFIG_PATH),
        os.getenv("APP_ENVIRONMENT_PATH", DEFAULT_ENVIRONMENT_PATH),
    )
) -> None:
    global _config
    if _config is not None:
        return
    _config = ConfigFactory.create_config_object(ApplicationConfig, config_paths)
    init_common_config_models(
        logger_config_value=_config.logger,
        postgres_config_value=_config.db.postgres,
        file_storage_config_value=_config.file_storage,
    )


def get_config() -> ApplicationConfig:
    global _config
    if _config is None:
        raise ValueError("Config is not initialized.")
    return _config
