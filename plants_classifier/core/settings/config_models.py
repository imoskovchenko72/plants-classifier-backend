from typing import Optional

from pydantic import BaseModel, field_validator

from plants_classifier.common import FileStorageConfig, LoggerConfig, PostgresConfig


class PlantariumConfig(BaseModel):
    path_mappings: dict[str, Optional[str]]

    @field_validator("path_mappings")
    @classmethod
    def validate_star_is_last_node_in_key_path(
        cls, paths: dict[str, Optional[str]]
    ) -> dict[str, Optional[str]]:
        for path in paths.keys():
            if "*" in path.split("/")[:-1]:
                raise ValueError("Star should be last node in path")

        return paths

    @field_validator("path_mappings")
    @classmethod
    def validate_all_paths_in_keys_are_different(
        cls, paths: dict[str, Optional[str]]
    ) -> dict[str, Optional[str]]:
        converted_paths = list()
        original_paths = sorted(paths.keys())
        for path in original_paths:
            converted_path = list()
            for node in path.split("/"):
                if node.startswith("$"):
                    converted_path.append("$")
                else:
                    converted_path.append(node)
            converted_paths.append(converted_path)

        for i in range(len(converted_paths)):
            for j in range(i + 1, len(converted_paths)):
                path1 = converted_paths[i]
                path2 = converted_paths[j]
                is_same = True
                for k in range(min(len(path1), len(path2))):
                    if path1[k] == "*" or path2[k] == "*":
                        break
                    if path1[k] != path2[k] and path1[k] != "$" and path2[k] != "$":
                        is_same = False
                else:
                    if len(path1) != len(path2):
                        is_same = False

                if is_same:
                    raise ValueError(
                        f"keys {original_paths[i]} and {original_paths[j]} recognizes same node sequence"
                    )

        return paths

    @field_validator("path_mappings")
    @classmethod
    def validate_values_paths_are_correct(
        cls, paths: dict[str, Optional[str]]
    ) -> dict[str, Optional[str]]:
        for key, value in paths.items():
            if value is None:
                continue
            key_path_nodes = set(key.split("/"))
            value_nodes = value.split("/")
            if len(value_nodes) != 2:
                raise ValueError(
                    f"Value {value} should have format 'where_to_save/some_nodes_from_key'"
                )

            for saving_value in value_nodes[1].split(" "):
                if (
                    saving_value == "*" or saving_value.startswith("$")
                ) and saving_value not in key_path_nodes:
                    raise ValueError(
                        f"Unknown node {saving_value} for pair {key}: {value}"
                    )
        return paths


class ClassificationConfigs(BaseModel):
    characteristics_importance_coefficients: dict[str, float]


class DbConfigs(BaseModel):
    postgres: PostgresConfig


class WebServerConfigs(BaseModel):
    host: str
    port: int


class RestApiConfigs(BaseModel):
    version: str
    path_prefix: str
    allow_origins: list[str]


class ApplicationConfig(BaseModel):
    logger: LoggerConfig
    db: DbConfigs
    classification: ClassificationConfigs
    plantarium: PlantariumConfig
    web_server: WebServerConfigs
    rest_api: RestApiConfigs
    file_storage: FileStorageConfig
