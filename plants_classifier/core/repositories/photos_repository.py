from typing import Optional, Sequence

from sqlalchemy import delete, select
from sqlalchemy.orm import Session

from plants_classifier.common import IRepository, with_session
from plants_classifier.core.db_models import PhotoModel


class PhotosRepository(IRepository[PhotoModel]):
    @with_session()
    def build(self, **kwargs) -> PhotoModel:
        return PhotoModel(**kwargs)

    @with_session()
    def create(self, db_session: Session, **kwargs) -> PhotoModel:
        model = PhotoModel(**kwargs)
        db_session.add(model)
        db_session.flush([model])
        return model

    @with_session()
    def save(self, model: PhotoModel, db_session: Session) -> None:
        db_session.add(model)
        db_session.flush([model])

    @with_session()
    def delete(self, model: PhotoModel, db_session: Session) -> None:
        query = delete(PhotoModel).where(PhotoModel.id == model.id)
        db_session.execute(query)

    @with_session()
    def all(self, db_session: Session) -> Sequence[PhotoModel]:
        query = select(PhotoModel).order_by(PhotoModel.created_at)
        return db_session.scalars(query).all()

    @with_session()
    def all_by_plant_id(
        self, plant_id: int, db_session: Session
    ) -> Sequence[PhotoModel]:
        query = (
            select(PhotoModel)
            .filter_by(plant_id=plant_id)
            .order_by(PhotoModel.created_at)
        )
        return db_session.scalars(query).all()

    @with_session()
    def get_by_id(self, id: str, db_session: Session) -> Optional[PhotoModel]:
        query = select(PhotoModel).filter_by(id=id)
        return db_session.scalars(query).first()
