from typing import Optional, Sequence

from sqlalchemy import delete, select
from sqlalchemy.orm import Session

from plants_classifier.common import IRepository, with_session
from plants_classifier.core.constants import TaxonType
from plants_classifier.core.db_models import TaxonModel


class TaxonRepository(IRepository[TaxonModel]):
    def build(self, **kwargs) -> TaxonModel:
        return TaxonModel(**kwargs)

    @with_session()
    def create(self, db_session: Session, **kwargs) -> TaxonModel:
        taxon_model = TaxonModel(**kwargs)
        db_session.add(taxon_model)
        db_session.flush([taxon_model])
        return taxon_model

    @with_session()
    def save(self, model: TaxonModel, db_session: Session) -> None:
        db_session.add(model)
        db_session.flush([model])

    @with_session()
    def delete(self, model: TaxonModel, db_session: Session) -> None:
        query = delete(TaxonModel).where(TaxonModel.id == model.id)
        db_session.execute(query)

    @with_session()
    def all(self, db_session: Session) -> Sequence[TaxonModel]:
        query = select(TaxonModel).order_by(TaxonModel.name)
        return db_session.scalars(query).all()

    @with_session()
    def get_by_id(self, id: int, db_session: Session) -> Optional[TaxonModel]:
        query = select(TaxonModel).filter_by(id=id)
        return db_session.scalars(query).first()

    @with_session()
    def get_by_name_and_type(
        self, name: str, taxon_type: TaxonType, db_session: Session
    ) -> Optional[TaxonModel]:
        query = select(TaxonModel).filter_by(name=name, type=taxon_type.value)
        return db_session.scalars(query).first()
