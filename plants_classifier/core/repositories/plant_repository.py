from typing import Optional, Sequence

from sqlalchemy import delete, select
from sqlalchemy.orm import Session

from plants_classifier.common import IRepository, with_session
from plants_classifier.core.db_models import PlantModel
from plants_classifier.core.dto import TaxonomyFilter

from .taxon_repository import TaxonRepository


class PlantRepository(IRepository[PlantModel]):
    def build(self, **kwargs) -> PlantModel:
        return PlantModel(**kwargs)

    @with_session()
    def create(self, db_session: Session, **kwargs) -> PlantModel:
        plant_model = PlantModel(**kwargs)
        db_session.add(plant_model)
        db_session.flush([plant_model])
        return plant_model

    @with_session()
    def save(self, model: PlantModel, db_session: Session) -> None:
        db_session.add(model)
        db_session.flush([model])

    @with_session()
    def delete(self, model: PlantModel, db_session: Session) -> None:
        query = delete(PlantModel).where(PlantModel.id == model.id)
        db_session.execute(query)

    @with_session()
    def all(self, db_session: Session) -> Sequence[PlantModel]:
        query = select(PlantModel).order_by(PlantModel.name)
        return db_session.scalars(query).all()

    @with_session()
    def all_with_taxonomy(
        self, taxonomy: Optional[TaxonomyFilter], db_session: Session
    ) -> Sequence[PlantModel]:
        query = select(PlantModel)
        if taxonomy is not None and taxonomy.taxon_id is not None:
            taxon = TaxonRepository().get_by_id(
                taxonomy.taxon_id, db_session=db_session
            )
            if taxon is None:
                return []
            query = query.filter_by(**{f"{taxon.type}_id": taxon.id})

        query = query.order_by(PlantModel.name).order_by(PlantModel.name)
        return db_session.scalars(query).all()

    @with_session()
    def get_by_id(self, id: int, db_session: Session) -> Optional[PlantModel]:
        query = select(PlantModel).filter_by(id=id)
        return db_session.scalars(query).first()

    @with_session()
    def get_by_name(self, name: str, db_session: Session) -> Optional[PlantModel]:
        query = select(PlantModel).filter_by(name=name)
        return db_session.scalars(query).first()
