from typing import Sequence

from sqlalchemy import delete, select
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import func

from plants_classifier.common import IRepository, with_session
from plants_classifier.core.constants import PlantCharacteristicType
from plants_classifier.core.db_models import CharacteristicsSimilarityModel


class CharacteristicsSimilarityRepository(IRepository[CharacteristicsSimilarityModel]):
    def build(self, **kwargs) -> CharacteristicsSimilarityModel:
        return CharacteristicsSimilarityModel(**kwargs)

    @with_session()
    def create(self, db_session: Session, **kwargs) -> CharacteristicsSimilarityModel:
        model = CharacteristicsSimilarityModel(**kwargs)
        db_session.add(model)
        db_session.flush([model])
        return model

    @with_session()
    def save(self, model: CharacteristicsSimilarityModel, db_session: Session) -> None:
        db_session.add(model)
        db_session.flush([model])

    @with_session()
    def delete(
        self, model: CharacteristicsSimilarityModel, db_session: Session
    ) -> None:
        query = delete(CharacteristicsSimilarityModel).where(
            CharacteristicsSimilarityModel.id == model.id
        )
        db_session.execute(query)

    @with_session()
    def all(self, db_session: Session) -> Sequence[CharacteristicsSimilarityModel]:
        query = select(CharacteristicsSimilarityModel)
        return db_session.scalars(query).all()

    @with_session()
    def get_max_count_by_characteristic_type(
        self, characteristic_type: PlantCharacteristicType, db_session: Session
    ) -> int:
        value = (
            db_session.query(func.max(CharacteristicsSimilarityModel.count))
            .filter_by(characteristic_type=characteristic_type.value)
            .scalar()
            or 0
        )
        return value

    @with_session()
    def get_characteristics_pair_count(
        self,
        characteristic_type: PlantCharacteristicType,
        first_value: str,
        second_value: str,
        db_session: Session,
    ) -> int:
        query = select(CharacteristicsSimilarityModel).filter_by(
            characteristic_type=characteristic_type.value,
            first_value=min(first_value, second_value),
            second_value=max(first_value, second_value),
        )
        model = db_session.scalars(query).first()
        if model:
            return model.count
        return 0

    @with_session()
    def increment_characteristics_pair_count(
        self,
        characteristic_type: PlantCharacteristicType,
        first_value: str,
        second_value: str,
        db_session: Session,
    ):
        self.update_count(
            characteristic_type=characteristic_type,
            first_value=first_value,
            second_value=second_value,
            step=1,
            db_session=db_session,
        )

    @with_session()
    def decrement_characteristics_pair_count(
        self,
        characteristic_type: PlantCharacteristicType,
        first_value: str,
        second_value: str,
        db_session: Session,
    ):
        self.update_count(
            characteristic_type=characteristic_type,
            first_value=first_value,
            second_value=second_value,
            step=-1,
            db_session=db_session,
        )

    @with_session()
    def update_count(
        self,
        characteristic_type: PlantCharacteristicType,
        first_value: str,
        second_value: str,
        step: int,
        db_session: Session,
    ):
        query = select(CharacteristicsSimilarityModel).filter_by(
            characteristic_type=characteristic_type.value,
            first_value=min(first_value, second_value),
            second_value=max(first_value, second_value),
        )
        model = db_session.scalars(query).first()
        if model:
            model.count += step
            self.save(model, db_session=db_session)
        else:
            self.create(
                characteristic_type=characteristic_type.value,
                first_value=min(first_value, second_value),
                second_value=max(first_value, second_value),
                count=step,
                db_session=db_session,
            )
