from typing import Sequence

from sqlalchemy import delete, select
from sqlalchemy.orm import Session

from plants_classifier.common import IRepository, with_session
from plants_classifier.core.constants import PlantCharacteristicType
from plants_classifier.core.db_models import CharacteristicsModel


class CharacteristicsRepository(IRepository[CharacteristicsModel]):
    @with_session()
    def build(self, **kwargs) -> CharacteristicsModel:
        return CharacteristicsModel(**kwargs)

    @with_session()
    def create(self, db_session: Session, **kwargs) -> CharacteristicsModel:
        model = CharacteristicsModel(**kwargs)
        db_session.add(model)
        db_session.flush([model])
        return model

    @with_session()
    def save(self, model: CharacteristicsModel, db_session: Session) -> None:
        db_session.add(model)
        db_session.flush([model])

    @with_session()
    def delete(self, model: CharacteristicsModel, db_session: Session) -> None:
        query = delete(CharacteristicsModel).where(CharacteristicsModel.id == model.id)
        db_session.execute(query)

    @with_session()
    def all(self, db_session: Session) -> Sequence[CharacteristicsModel]:
        query = select(CharacteristicsModel)
        return db_session.scalars(query).all()

    @with_session()
    def get_all_by_characteristic_type(
        self, characteristic_type: PlantCharacteristicType, db_session: Session
    ) -> list[str]:
        query = select(CharacteristicsModel).filter_by(
            characteristic_type=characteristic_type.value
        )
        return [item.value for item in db_session.scalars(query).all()]

    @with_session()
    def maybe_create(
        self,
        characteristic_type: PlantCharacteristicType,
        value: str,
        db_session: Session,
    ) -> CharacteristicsModel:
        """
        Same as create() but does not raise error if such characteristic already saved
        """

        query = select(CharacteristicsModel).filter_by(
            characteristic_type=characteristic_type.value, value=value
        )
        characteristic = db_session.scalars(query).first()
        if characteristic:
            return characteristic

        return self.create(
            characteristic_type=characteristic_type.value,
            value=value,
            db_session=db_session,
        )
