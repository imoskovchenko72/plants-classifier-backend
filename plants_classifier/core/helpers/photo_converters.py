from io import BytesIO

from PIL import Image


def get_reduced_image(photo_bytes: bytes) -> bytes:
    image = Image.open(BytesIO(photo_bytes))

    compression_coefficient = max(image.height, image.width) / 200
    new_width = int(image.width / compression_coefficient)
    new_height = int(image.height / compression_coefficient)

    image.thumbnail((new_width, new_height))

    bytes_stream = BytesIO()
    image.save(bytes_stream, format="JPEG")

    return bytes_stream.getvalue()
