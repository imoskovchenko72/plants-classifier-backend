from typing import TypeVar

from plants_classifier.core.constants import UNLIMITED

T = TypeVar("T")


def get_page_part(data: list[T], limit: int = UNLIMITED, offset: int = 0) -> list[T]:
    if offset >= len(data):
        return []

    start = offset
    end = len(data) if limit == UNLIMITED else min(len(data), offset + limit)

    return data[start:end]
