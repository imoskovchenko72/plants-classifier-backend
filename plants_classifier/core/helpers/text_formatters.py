def get_normalized_string(s: str) -> str:
    return s.strip().lower().replace("ё", "е")


def split_text(text: str) -> list[str]:
    punctuation_symbols = ".,;:()!?"
    for symbol in punctuation_symbols:
        text = text.replace(symbol, " ")
    return [get_normalized_string(word) for word in text.split(" ") if len(word) != 0]
