from sqlalchemy.orm import Session

from plants_classifier.common import get_file_storage, with_session
from plants_classifier.core.constants import PlantCharacteristicType, TaxonType
from plants_classifier.core.db_models import PlantModel
from plants_classifier.core.dto import (
    OutputPlant,
    OutputShortTaxonInfo,
    Plant,
    PlantCharacteristics,
    ShortOutputPlantInfo,
)
from plants_classifier.core.repositories import (
    CharacteristicsRepository,
    CharacteristicsSimilarityRepository,
    PhotosRepository,
    TaxonRepository,
)


@with_session()
def update_taxa(
    plant_dto: Plant, plant_db_model: PlantModel, db_session: Session
) -> None:
    taxon_repository = TaxonRepository()

    for taxon_type in TaxonType:
        taxon_name = plant_dto.dict().get(taxon_type.value, None)
        if taxon_name is not None:
            taxon_model = taxon_repository.get_by_name_and_type(
                name=taxon_name, taxon_type=taxon_type, db_session=db_session
            )
            if taxon_model is None:
                taxon_model = taxon_repository.build(
                    name=taxon_name, type=taxon_type.value
                )
                taxon_repository.save(model=taxon_model, db_session=db_session)
            setattr(plant_db_model, f"{taxon_type.value}_id", taxon_model.id)


@with_session()
def update_characteristics_similarity(
    plant_dto: Plant, plant_db_model: PlantModel, db_session: Session
) -> None:
    characteristics_similarity_repository = CharacteristicsSimilarityRepository()
    characteristics_repository = CharacteristicsRepository()

    for characteristic_type in PlantCharacteristicType:
        characteristics_values = plant_dto.dict().get(characteristic_type.value, None)
        if characteristics_values is None:
            continue

        for value in characteristics_values:
            characteristics_repository.maybe_create(
                characteristic_type=characteristic_type,
                value=value,
                db_session=db_session,
            )

        for first_value in characteristics_values:
            for second_value in characteristics_values:
                if first_value != second_value:
                    characteristics_similarity_repository.increment_characteristics_pair_count(
                        characteristic_type=characteristic_type,
                        first_value=first_value,
                        second_value=second_value,
                        db_session=db_session,
                    )
            characteristics_similarity_repository.update_count(
                characteristic_type=characteristic_type,
                first_value=first_value,
                second_value=first_value,
                step=2 * (len(characteristics_values) - 1),
                db_session=db_session,
            )
        setattr(plant_db_model, characteristic_type.value, characteristics_values)


@with_session()
def remove_characteristics(plant_db_model: PlantModel, db_session: Session) -> None:
    characteristics_similarity_repository = CharacteristicsSimilarityRepository()
    model_dict = plant_db_model.to_dict()

    for characteristic_type in PlantCharacteristicType:
        characteristics_values = model_dict.get(characteristic_type.value, None)
        if characteristics_values is None:
            continue

        for first_value in characteristics_values:
            for second_value in characteristics_values:
                if first_value != second_value:
                    characteristics_similarity_repository.decrement_characteristics_pair_count(
                        characteristic_type=characteristic_type,
                        first_value=first_value,
                        second_value=second_value,
                        db_session=db_session,
                    )
            characteristics_similarity_repository.update_count(
                characteristic_type=characteristic_type,
                first_value=first_value,
                second_value=first_value,
                step=2 * (1 - len(characteristics_values)),
                db_session=db_session,
            )


@with_session()
def remove_photos(plant_id: int, db_session: Session) -> None:
    photo_repository = PhotosRepository()
    photos = photo_repository.all_by_plant_id(plant_id=plant_id, db_session=db_session)
    file_storage = get_file_storage()
    for photo in photos:
        file_storage.delete(photo.id)
        photo_repository.delete(photo)


@with_session()
def convert_plant_db_model_to_dto(
    plant_model: PlantModel, db_session: Session
) -> OutputPlant:
    output_plant = OutputPlant(
        id=plant_model.id,
        name=plant_model.name,
        russian_names=plant_model.russian_names,
        is_endangered=plant_model.is_endangered,
        description=plant_model.description,
    )
    plant_model_dict = plant_model.to_dict()
    taxon_repository = TaxonRepository()

    for taxon_type in TaxonType:
        taxon_id = plant_model_dict.get(f"{taxon_type.value}_id", None)
        if taxon_id is not None:
            taxon_model = taxon_repository.get_by_id(id=taxon_id, db_session=db_session)
            setattr(
                output_plant,
                taxon_type.value,
                OutputShortTaxonInfo(id=taxon_model.id, name=taxon_model.name),
            )

    for characteristic_type in PlantCharacteristicType:
        setattr(
            output_plant,
            characteristic_type.value,
            plant_model_dict.get(characteristic_type.value, None),
        )
    return output_plant


@with_session()
def convert_plant_model_to_short_info(
    plant_model: PlantModel, db_session: Session
) -> ShortOutputPlantInfo:
    photos = PhotosRepository().all_by_plant_id(
        plant_id=plant_model.id, db_session=db_session
    )
    return ShortOutputPlantInfo(
        id=plant_model.id,
        name=plant_model.name,
        is_endangered=plant_model.is_endangered,
        russian_name=plant_model.russian_names[0]
        if plant_model.russian_names
        else None,
        photo_id=photos[0].id if photos else None,
    )


def get_plant_characteristics_from_db_model(plant: PlantModel) -> PlantCharacteristics:
    plant_dict = plant.to_dict()
    return PlantCharacteristics(**plant_dict, ignore_extra_fields=True)
