"""coordinates model

Revision ID: 3e3fa69bfbe6
Revises: 25b802e83c03
Create Date: 2024-03-12 17:18:53.508751

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "3e3fa69bfbe6"
down_revision: Union[str, None] = "25b802e83c03"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "coordinates",
        sa.Column("id", sa.BIGINT(), autoincrement=True, nullable=False),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.text("CURRENT_TIMESTAMP"),
            nullable=False,
        ),
        sa.Column("date", sa.DATE(), nullable=True),
        sa.Column("description", sa.TEXT(), nullable=True),
        sa.Column("plant_id", sa.BIGINT(), nullable=False),
        sa.Column("latitude", sa.DECIMAL(), nullable=False),
        sa.Column("longitude", sa.DECIMAL(), nullable=False),
        sa.ForeignKeyConstraint(["plant_id"], ["plants.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("coordinates")
    # ### end Alembic commands ###
