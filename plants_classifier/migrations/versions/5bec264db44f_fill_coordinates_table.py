"""fill coordinates table

Revision ID: 5bec264db44f
Revises: 41c4789463e3
Create Date: 2024-05-22 16:37:01.765549

"""
import csv
import datetime
import os
import pathlib
from functools import lru_cache
from typing import Sequence, Union

from alembic import op
from sqlalchemy.orm import sessionmaker

from plants_classifier.common.base_db_model import metadata
from plants_classifier.core.db_models import CoordinatesModel
from plants_classifier.core.repositories import PlantRepository

# revision identifiers, used by Alembic.
revision: str = "5bec264db44f"
down_revision: Union[str, None] = "41c4789463e3"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


@lru_cache(maxsize=1)
def get_initial_data() -> list[dict]:
    coordinates = []
    path = os.path.join(
        pathlib.Path(__file__).parent.resolve(),
        "..",
        "..",
        "data",
        "coordinates.csv",
    )

    with open(path, newline="", encoding="cp1251") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row["scientificName\n"]
            latitude = row["decimalLatitude"]
            longitude = row["decimalLongitude"]
            description = row["verbatimLocality"]
            date = row["eventDate"]
            if not name or not latitude or not longitude:
                continue

            if date:
                date_item = date.split("T")[0]
                date = datetime.date(*map(int, date_item.split("-")))
            else:
                date = None

            name_items = name.lower().replace("(", " ").replace(")", " ").split()
            real_name = " ".join(name_items[0:2])

            coordinates.append(
                {
                    "name": real_name,
                    "latitude": latitude,
                    "longitude": longitude,
                    "description": description,
                    "date": date,
                }
            )
    return coordinates


def upgrade() -> None:
    data = get_initial_data()

    name_to_id = {}

    connection = op.get_bind()
    session_maker = sessionmaker(bind=connection)

    with session_maker() as session:
        plants = PlantRepository().all(db_session=session)
        for plant in plants:
            name_to_id[plant.name] = plant.id

    prepared_data = []

    for coordinate in data:
        plant_id = name_to_id.get(coordinate["name"])
        if plant_id is None:
            continue
        prepared_data.append(
            {
                "plant_id": plant_id,
                "latitude": coordinate["latitude"],
                "longitude": coordinate["longitude"],
                "description": coordinate["description"],
                "date": coordinate["date"],
            }
        )

    op.bulk_insert(metadata.tables[CoordinatesModel.__tablename__], prepared_data)


def downgrade() -> None:
    op.execute("TRUNCATE TABLE coordinates;")
