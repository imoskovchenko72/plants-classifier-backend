"""fill data

Revision ID: 25b802e83c03
Revises: 1cd7a7e4ad9f
Create Date: 2024-02-13 23:09:05.986983

"""
import os
import pathlib
from collections import defaultdict
from functools import lru_cache
from typing import Sequence, Union

import yaml
from alembic import op
from sqlalchemy import select

from plants_classifier.common.base_db_model import metadata
from plants_classifier.core.constants import PlantCharacteristicType, TaxonType
from plants_classifier.core.db_models import (
    CharacteristicsModel,
    CharacteristicsSimilarityModel,
    PlantModel,
    TaxonModel,
)

# revision identifiers, used by Alembic.
revision: str = "25b802e83c03"
down_revision: Union[str, None] = "1cd7a7e4ad9f"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


@lru_cache(maxsize=1)
def get_initial_data() -> list[dict]:
    plants = []
    for i in range(26):
        path = os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "..",
            "..",
            "data",
            f"initial_data_batch{i}.yaml",
        )
        with open(path, "r", encoding="utf-8") as file:
            plants += yaml.safe_load(file)

    return plants


@lru_cache(maxsize=1)
def get_endangered_species() -> set[str]:
    path = os.path.join(
        pathlib.Path(__file__).parent.resolve(),
        "..",
        "..",
        "data",
        "endangered_species.txt",
    )
    species = []
    with open(path, "r", encoding="utf-8") as file:
        for line in file:
            species.append(line[:-1])
    return set(species)


def upgrade() -> None:
    plants = get_initial_data()
    endangered_species = get_endangered_species()

    known_taxa = set()
    known_characteristics = set()
    known_characteristics_pairs = defaultdict(int)

    for plant in plants:
        for taxon_type in TaxonType:
            name = plant.get(taxon_type.value, None)
            if name is not None:
                known_taxa.add((taxon_type.value, name))

        for characteristic_type in PlantCharacteristicType:
            characteristics_list = plant.get(characteristic_type.value, None)
            if characteristics_list is None:
                continue
            for characteristic in characteristics_list:
                known_characteristics.add((characteristic_type.value, characteristic))

            for first_value in characteristics_list:
                for second_value in characteristics_list:
                    if first_value != second_value:
                        known_characteristics_pairs[
                            (
                                characteristic_type.value,
                                min(first_value, second_value),
                                max(first_value, second_value),
                            )
                        ] += 1
                known_characteristics_pairs[
                    (characteristic_type.value, first_value, first_value)
                ] += 2 * (len(characteristics_list) - 1)

    op.bulk_insert(
        metadata.tables[TaxonModel.__tablename__],
        [
            {"type": taxon_tuple[0], "name": taxon_tuple[1]}
            for taxon_tuple in known_taxa
        ],
    )
    op.bulk_insert(
        metadata.tables[CharacteristicsModel.__tablename__],
        [
            {"characteristic_type": char_tuple[0], "value": char_tuple[1]}
            for char_tuple in known_characteristics
        ],
    )
    op.bulk_insert(
        metadata.tables[CharacteristicsSimilarityModel.__tablename__],
        [
            {
                "characteristic_type": key[0],
                "first_value": key[1],
                "second_value": key[2],
                "count": value,
            }
            for key, value in known_characteristics_pairs.items()
        ],
    )

    connection = op.get_bind()
    inserted_taxa = connection.execute(select(TaxonModel)).all()
    taxon_to_id = {}
    for taxon in inserted_taxa:
        taxon_to_id[(taxon[2], taxon[1])] = taxon[0]

    for plant in plants:
        plant["is_endangered"] = plant["name"] in endangered_species
        for taxon_type in TaxonType:
            name = plant.get(taxon_type.value, None)
            plant[f"{taxon_type.value}_id"] = taxon_to_id.get(
                (taxon_type.value, name), None
            )
            plant.pop(taxon_type.value, None)

    op.bulk_insert(metadata.tables[PlantModel.__tablename__], plants)


def downgrade() -> None:
    op.execute("TRUNCATE TABLE plants;")
    op.execute("TRUNCATE TABLE taxa CASCADE;")
    op.execute("TRUNCATE TABLE characteristics_similarity;")
    op.execute("TRUNCATE TABLE characteristics;")
