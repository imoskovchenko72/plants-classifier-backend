#!/bin/sh

export export PYTHONPATH="/app"

cd ./plants_classifier/migrations
alembic upgrade head
cd ..
python3 ./scripts/fill_photos.py
python3 __main__.py
