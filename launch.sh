#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Usage: $0 <version> <port>"
  exit 1
fi

version=$1
port=$2

docker build -t classifier-backend-v$version .

docker run -d -p $port:80 -e APP_ENVIRONMENT_PATH='/app/plants_classifier/config/environment.yaml' -v /home/storage_data:/app/storage_data classifier-backend-v$version
