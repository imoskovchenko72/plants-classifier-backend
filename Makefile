format:
	isort . && black .
lint:
	flake8 --config setup.cfg . && mypy .
create-migration:
	cd ./plants_classifier/migrations && alembic revision --autogenerate -m "$(MESSAGE)"
migrate:
	cd ./plants_classifier/migrations && alembic upgrade head
test:
	pytest ./tests -s